﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void csDestroyEffect::Update()
extern void csDestroyEffect_Update_m653EBF52C9C3F6A73011A5FD724BAF81A16A0C85 (void);
// 0x00000002 System.Void csDestroyEffect::.ctor()
extern void csDestroyEffect__ctor_m12E4742C6E2950D223C4B1F24049F51C44F8A134 (void);
// 0x00000003 System.Void csMouseOrbit::Start()
extern void csMouseOrbit_Start_mAEA48A24D38C91EDBDF830718C1C16AB2AEC3841 (void);
// 0x00000004 System.Void csMouseOrbit::LateUpdate()
extern void csMouseOrbit_LateUpdate_mC94A662A0BB3AA2AEA85555D599BA304A95DFF0F (void);
// 0x00000005 System.Single csMouseOrbit::ClampAngle(System.Single,System.Single,System.Single)
extern void csMouseOrbit_ClampAngle_m1278F467346C0B15E36A2F3AA63FB85C14924325 (void);
// 0x00000006 System.Void csMouseOrbit::.ctor()
extern void csMouseOrbit__ctor_m78926A64F3CBD11595E1C30BF66D9C469DC9778C (void);
// 0x00000007 System.Void csParticleEffectPackLightControl::Update()
extern void csParticleEffectPackLightControl_Update_mC85A40D3234927737C226DF87126A91A7A58C02A (void);
// 0x00000008 System.Void csParticleEffectPackLightControl::.ctor()
extern void csParticleEffectPackLightControl__ctor_m9724C65D8057AA3C00BFAD52BC7144BB70BD761B (void);
// 0x00000009 System.Void csParticleMove::Update()
extern void csParticleMove_Update_m568A81FC8597439ED0E2BC7D3F87C2802DA62E2A (void);
// 0x0000000A System.Void csParticleMove::.ctor()
extern void csParticleMove__ctor_m125C12E4DD1294FF1969270296B590DD1632A9F6 (void);
// 0x0000000B System.Void csShowAllEffect::Start()
extern void csShowAllEffect_Start_m06A3C35FDFF54F7E8519B976E06C505E5149BF9F (void);
// 0x0000000C System.Void csShowAllEffect::Update()
extern void csShowAllEffect_Update_mEF27DC015899FF9BAEA45A7E7EFF05C0F1E491D5 (void);
// 0x0000000D System.Void csShowAllEffect::.ctor()
extern void csShowAllEffect__ctor_mA7021BB8AD67ED68AC384A503C4E4489CC7A9557 (void);
// 0x0000000E System.Void ApplyRandomMaterial::Start()
extern void ApplyRandomMaterial_Start_m91E40AF66BE87232F9FA45151E05244CE8DBA902 (void);
// 0x0000000F System.Void ApplyRandomMaterial::GenerateRandomColorForMaterial()
extern void ApplyRandomMaterial_GenerateRandomColorForMaterial_mAB28DC52B1EF910C4F669BEBE6A74A1C15B7656F (void);
// 0x00000010 UnityEngine.Color ApplyRandomMaterial::GetRandomColor()
extern void ApplyRandomMaterial_GetRandomColor_m799BCF0423323BD9554E4CFDF786272F2ADD57B6 (void);
// 0x00000011 System.Void ApplyRandomMaterial::.ctor()
extern void ApplyRandomMaterial__ctor_m42C78367767FC9719C3219F244E364E4C973D23C (void);
// 0x00000012 System.Void AutoPlacementOfObjectsInPlane::Awake()
extern void AutoPlacementOfObjectsInPlane_Awake_m169162C493C5EA0D640F28B037612962F602D841 (void);
// 0x00000013 System.Void AutoPlacementOfObjectsInPlane::PlaneChanged(UnityEngine.XR.ARFoundation.ARPlanesChangedEventArgs)
extern void AutoPlacementOfObjectsInPlane_PlaneChanged_mB531E4B70DC8314AA2A23944DE17C394ECD1DC47 (void);
// 0x00000014 System.Void AutoPlacementOfObjectsInPlane::Dismiss()
extern void AutoPlacementOfObjectsInPlane_Dismiss_mB0F9A2FE5235F0D00849CD3F0806E3B3BF1E86B0 (void);
// 0x00000015 System.Void AutoPlacementOfObjectsInPlane::.ctor()
extern void AutoPlacementOfObjectsInPlane__ctor_mF65F9F4E84F26A189037220FBBCEA9F06A1D3AC4 (void);
// 0x00000016 System.Void BoneTracker::Start()
extern void BoneTracker_Start_m9816EF062079FCFB903D3449025D8D851561B969 (void);
// 0x00000017 System.Void BoneTracker::Update()
extern void BoneTracker_Update_m81BFEC6F2CC4A9928A9EEFD3F17B28BA67533C6A (void);
// 0x00000018 System.Void BoneTracker::.ctor()
extern void BoneTracker__ctor_m0F3FF30CCA38BC9227468B3909CE904CCD45995E (void);
// 0x00000019 UnityEngine.Vector4 BuildRig::GetColumn(System.Int32,System.Int32)
extern void BuildRig_GetColumn_m94CC96EFF4F5DCDB5C146E32B729DD96D612CFEC (void);
// 0x0000001A System.Void BuildRig::Build()
extern void BuildRig_Build_mCB61BD2B563F71CA0A64BF857E6079F58EE73CDE (void);
// 0x0000001B System.Void BuildRig::Start()
extern void BuildRig_Start_m4073A620B3AC6CC5BA6E8503E03081EA2D6ABC19 (void);
// 0x0000001C UnityEngine.Quaternion BuildRig::ExtractRotation(UnityEngine.Matrix4x4)
extern void BuildRig_ExtractRotation_mA8B181D7FA399BCF35D6E4E658F9A27D7BBEBE74 (void);
// 0x0000001D UnityEngine.Vector3 BuildRig::ExtractPosition(UnityEngine.Matrix4x4)
extern void BuildRig_ExtractPosition_m118A7A6BBD1457F297AEE7003A66E8F1C586685D (void);
// 0x0000001E UnityEngine.Vector3 BuildRig::ExtractScale(UnityEngine.Matrix4x4)
extern void BuildRig_ExtractScale_m771FF0353BB59143F43E235A25FFFA895D149C11 (void);
// 0x0000001F System.Void BuildRig::FromMatrix(UnityEngine.Transform,UnityEngine.Matrix4x4)
extern void BuildRig_FromMatrix_mB8870173E0D30E02A968A40A8AF8B023380AAF7F (void);
// 0x00000020 System.Void BuildRig::.ctor()
extern void BuildRig__ctor_mA88AC25FBF830B5A90B0FFEFBAED393FD744DC01 (void);
// 0x00000021 System.Void BuildRig::.cctor()
extern void BuildRig__cctor_mD3E6B57DFFF1E26C9F3F7367F0F7C8973813B178 (void);
// 0x00000022 System.Void CaptureAreaManager::Update()
extern void CaptureAreaManager_Update_mFBF356AEFA485044F2904CF6FA732733E8160E3F (void);
// 0x00000023 System.Void CaptureAreaManager::.ctor()
extern void CaptureAreaManager__ctor_m0D55DE70EED0A7C456336FCD09FDDA0FFCF8717E (void);
// 0x00000024 System.Void EffectsManager::Start()
extern void EffectsManager_Start_m176E81CD4BECCCA0766BBE97C4001C00EB957D04 (void);
// 0x00000025 System.Void EffectsManager::TogglePlaneDetection()
extern void EffectsManager_TogglePlaneDetection_mD82C0637F3ABF8BCD6C540D7A88A9FA55FFDCA14 (void);
// 0x00000026 System.Void EffectsManager::ToggleLights()
extern void EffectsManager_ToggleLights_mCF8EBF0DE4AA7251077A636AAE888A3F7D0FFD0E (void);
// 0x00000027 System.Void EffectsManager::ToggleShadows()
extern void EffectsManager_ToggleShadows_m0DDAD84F115F68710E2E2A54174B18476AA6E379 (void);
// 0x00000028 System.Void EffectsManager::.ctor()
extern void EffectsManager__ctor_m20B31BB9680029000B91CCE1F74B4E154856CB9A (void);
// 0x00000029 System.Void EyeTracker::Awake()
extern void EyeTracker_Awake_m54A20B34D9CFAB9A2A03ECC58A76A73420542898 (void);
// 0x0000002A System.Void EyeTracker::OnEnable()
extern void EyeTracker_OnEnable_mDB475A76F5D71784A3EF31E933AC0B09D3BA4205 (void);
// 0x0000002B System.Void EyeTracker::OnDisable()
extern void EyeTracker_OnDisable_m66C55BDEB8F7522F564CAAAF5C35EC91A47A44B7 (void);
// 0x0000002C System.Void EyeTracker::OnUpdated(UnityEngine.XR.ARFoundation.ARFaceUpdatedEventArgs)
extern void EyeTracker_OnUpdated_mF3BD6A7F4A1F0268CA6B4551FC604AA0F4596B64 (void);
// 0x0000002D System.Void EyeTracker::SetVisibility(System.Boolean)
extern void EyeTracker_SetVisibility_m63966D6BFE77487A6FFB32C6FDC0D8149C28BE7C (void);
// 0x0000002E System.Void EyeTracker::.ctor()
extern void EyeTracker__ctor_mADB9C25EF0F9FC27B15B549661138F1A8BA75200 (void);
// 0x0000002F System.Void EyeTrackerSupported::OnEnable()
extern void EyeTrackerSupported_OnEnable_m97588929026E0131073052AA36947D274E351333 (void);
// 0x00000030 System.Void EyeTrackerSupported::.ctor()
extern void EyeTrackerSupported__ctor_mD94A640B151070609B89C18036152280E410380A (void);
// 0x00000031 System.Void FaceController::Awake()
extern void FaceController_Awake_m912D2313AD97922CD6A32C36115CA957F3DF3C24 (void);
// 0x00000032 System.Void FaceController::SwapFaces()
extern void FaceController_SwapFaces_m26884D50B7A58F520017514182925D306E6CDB3E (void);
// 0x00000033 System.Void FaceController::ToggleTrackingFaces()
extern void FaceController_ToggleTrackingFaces_m1402FBAB1EB75B40E19863BA9C5B0692D40B61DA (void);
// 0x00000034 System.Void FaceController::.ctor()
extern void FaceController__ctor_mD465C8056CA9CE1C5A12DB31BE2AA6A0AF8079F5 (void);
// 0x00000035 System.Void FaceMaterial::.ctor()
extern void FaceMaterial__ctor_mD6C4F71CF959CB18D67F536BAABC30D7B3E75278 (void);
// 0x00000036 System.Void FeatureSupported::Start()
extern void FeatureSupported_Start_m7239FA98F3A9AA310DAF68974A918AC103C2FDA6 (void);
// 0x00000037 System.Void FeatureSupported::.ctor()
extern void FeatureSupported__ctor_mC383A584443CB51E32B9933CB193F3446FFF19E2 (void);
// 0x00000038 UnityEngine.XR.ARFoundation.ARHumanBodyManager HumanBodyTracker::get_HumanBodyManagers()
extern void HumanBodyTracker_get_HumanBodyManagers_mB6D61DFBBEF93E1A3C15776E22784FC52E80561C (void);
// 0x00000039 System.Void HumanBodyTracker::set_HumanBodyManagers(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTracker_set_HumanBodyManagers_mA2C6B4E92A9F474E4E8AD8940A0CB2E1C0935F73 (void);
// 0x0000003A UnityEngine.GameObject HumanBodyTracker::get_SkeletonPrefab()
extern void HumanBodyTracker_get_SkeletonPrefab_m1878E9CA6AF43C1D2C94A1DF623111C5884B18B4 (void);
// 0x0000003B System.Void HumanBodyTracker::set_SkeletonPrefab(UnityEngine.GameObject)
extern void HumanBodyTracker_set_SkeletonPrefab_m675784859DA8088A25917F4CCC2FA232169D0B55 (void);
// 0x0000003C System.Void HumanBodyTracker::OnEnable()
extern void HumanBodyTracker_OnEnable_m231972EA78E173D2A8175FDF68473FAE3B1AA6F6 (void);
// 0x0000003D System.Void HumanBodyTracker::OnDisable()
extern void HumanBodyTracker_OnDisable_mA30452BA079BE71990B6B7DB97C8C3BDCDEA0A1F (void);
// 0x0000003E System.Void HumanBodyTracker::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTracker_OnHumanBodiesChanged_m90A2571C20D330C6D9AAB144DE45A44392F4D261 (void);
// 0x0000003F System.Void HumanBodyTracker::UpdateClothes(HumanBoneController)
extern void HumanBodyTracker_UpdateClothes_m0955ED75A52C30FBC09BB251992DDBA3BA4147E3 (void);
// 0x00000040 System.Void HumanBodyTracker::.ctor()
extern void HumanBodyTracker__ctor_m454447211F32D1F1F9F366BFBB067CF676A2520E (void);
// 0x00000041 UnityEngine.XR.ARFoundation.ARHumanBodyManager HumanBodyTrackerLogging::get_HumanBodyManagers()
extern void HumanBodyTrackerLogging_get_HumanBodyManagers_mC42C0DB293C13BCA47FBEAF2EBE2A73623C7E4C1 (void);
// 0x00000042 System.Void HumanBodyTrackerLogging::set_HumanBodyManagers(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTrackerLogging_set_HumanBodyManagers_mDDAA3756715848EADF00F0D322B4A253C278DD73 (void);
// 0x00000043 UnityEngine.GameObject HumanBodyTrackerLogging::get_SkeletonPrefab()
extern void HumanBodyTrackerLogging_get_SkeletonPrefab_m7276D68CF3CDDCF33E1CAFE28B87A8D229D2E473 (void);
// 0x00000044 System.Void HumanBodyTrackerLogging::set_SkeletonPrefab(UnityEngine.GameObject)
extern void HumanBodyTrackerLogging_set_SkeletonPrefab_m6308DA30DE2CF3A2B6333921BAB24CF6FA511B3E (void);
// 0x00000045 System.Void HumanBodyTrackerLogging::Awake()
extern void HumanBodyTrackerLogging_Awake_m63E4BF740A2ACF9246C937081655F438BB0BCA0B (void);
// 0x00000046 System.Void HumanBodyTrackerLogging::DissmissWelcomePanel()
extern void HumanBodyTrackerLogging_DissmissWelcomePanel_m501BFB23493BC79AC798E9D922C4AA76E7DCE129 (void);
// 0x00000047 System.Void HumanBodyTrackerLogging::ToggleDebugPanel()
extern void HumanBodyTrackerLogging_ToggleDebugPanel_m699252C1CC7D8486D258BCBBB19E83557C09342D (void);
// 0x00000048 System.Void HumanBodyTrackerLogging::OnEnable()
extern void HumanBodyTrackerLogging_OnEnable_m8DE86567680D20DFC275035DF023AED68A6BFF3B (void);
// 0x00000049 System.Void HumanBodyTrackerLogging::OnDisable()
extern void HumanBodyTrackerLogging_OnDisable_m335AF011D77A29F26FF09F997F09B5C7CD4A0D40 (void);
// 0x0000004A System.Void HumanBodyTrackerLogging::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTrackerLogging_OnHumanBodiesChanged_m479546B4BDBF5DA95C7B89DD07C118B2C101F39E (void);
// 0x0000004B System.Void HumanBodyTrackerLogging::ApplyPowers(BoneTracker[])
extern void HumanBodyTrackerLogging_ApplyPowers_mA9B45984D0465F97CAC45FA3AF7CF48727679D77 (void);
// 0x0000004C System.Void HumanBodyTrackerLogging::.ctor()
extern void HumanBodyTrackerLogging__ctor_m6766028CE5DCAD48602EE144C3260FAE6AD974F3 (void);
// 0x0000004D System.Void HumanBodyTrackerUI::OnEnable()
extern void HumanBodyTrackerUI_OnEnable_m6F0BFABC6ADD3259F15A6B21F279A65CBD8B5FCC (void);
// 0x0000004E System.Void HumanBodyTrackerUI::Awake()
extern void HumanBodyTrackerUI_Awake_mCDB175AD6175D75031D36D99E3F518128E4E9F14 (void);
// 0x0000004F System.Void HumanBodyTrackerUI::Dismiss()
extern void HumanBodyTrackerUI_Dismiss_m276AE77DA2A2A42D6DBAE3C620048C411D16A7E8 (void);
// 0x00000050 System.Void HumanBodyTrackerUI::ToggleDebugging()
extern void HumanBodyTrackerUI_ToggleDebugging_mABD7025234BB295BD0B5058BD31B84462A8A3E94 (void);
// 0x00000051 System.Void HumanBodyTrackerUI::ToggleText(UnityEngine.UI.Text)
extern void HumanBodyTrackerUI_ToggleText_m9666E2FAF10FF936D8FBD27E917B2AC3822F4767 (void);
// 0x00000052 System.Void HumanBodyTrackerUI::ToggleOptions()
extern void HumanBodyTrackerUI_ToggleOptions_mA3850F930FE0A507FBFEE64EBC03A51A8BD9B432 (void);
// 0x00000053 System.Void HumanBodyTrackerUI::.ctor()
extern void HumanBodyTrackerUI__ctor_mB787568466F97D8332E1CB0D6AF893039912A173 (void);
// 0x00000054 UnityEngine.XR.ARFoundation.ARHumanBodyManager HumanBodyTrackerWithOptions::get_HumanBodyManagers()
extern void HumanBodyTrackerWithOptions_get_HumanBodyManagers_mE9E8745BF7AC07B5C55DA26263B3699D5F190735 (void);
// 0x00000055 System.Void HumanBodyTrackerWithOptions::set_HumanBodyManagers(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void HumanBodyTrackerWithOptions_set_HumanBodyManagers_mECEB734F8EC14090992CA3D25971B0F018AD23F3 (void);
// 0x00000056 UnityEngine.GameObject HumanBodyTrackerWithOptions::get_SkeletonPrefab()
extern void HumanBodyTrackerWithOptions_get_SkeletonPrefab_mD060B292640C829D092EEF77D9CE807B81372CC6 (void);
// 0x00000057 System.Void HumanBodyTrackerWithOptions::set_SkeletonPrefab(UnityEngine.GameObject)
extern void HumanBodyTrackerWithOptions_set_SkeletonPrefab_m86E92CEC9C1F9EAF1124F408AAFB88C226A92499 (void);
// 0x00000058 System.Void HumanBodyTrackerWithOptions::Awake()
extern void HumanBodyTrackerWithOptions_Awake_mE2E259C7D5457981640823520D857C2AEE71EBB9 (void);
// 0x00000059 System.Void HumanBodyTrackerWithOptions::OnEnable()
extern void HumanBodyTrackerWithOptions_OnEnable_m4655C2A1A7F42C5997C1D456529FD465345FA0CB (void);
// 0x0000005A System.Void HumanBodyTrackerWithOptions::DissmissWelcomePanel()
extern void HumanBodyTrackerWithOptions_DissmissWelcomePanel_mD5C78369BABD7B4B3589B33AA22A0C22EC8807D9 (void);
// 0x0000005B System.Void HumanBodyTrackerWithOptions::OnDisable()
extern void HumanBodyTrackerWithOptions_OnDisable_m78C5268D2F3CA147944ADCDAF11237B483F71CC2 (void);
// 0x0000005C System.Void HumanBodyTrackerWithOptions::OnCurveTimeSliderChanged(System.Single)
extern void HumanBodyTrackerWithOptions_OnCurveTimeSliderChanged_m4605791BFA1427B9E008CBD1E3DE7E1D98AAB586 (void);
// 0x0000005D System.Void HumanBodyTrackerWithOptions::OnMinVertexDistanceSliderChanged(System.Single)
extern void HumanBodyTrackerWithOptions_OnMinVertexDistanceSliderChanged_m7C93A64A7A0FFCD4DFB2E08DE5B4EE3BF98A36B7 (void);
// 0x0000005E System.Void HumanBodyTrackerWithOptions::ApplyChangesToCurve(HumanBoneController)
extern void HumanBodyTrackerWithOptions_ApplyChangesToCurve_m538E6B7AD8BBEAC27808C577411FB6BC9E9D4FC4 (void);
// 0x0000005F System.Void HumanBodyTrackerWithOptions::OnHumanBodiesChanged(UnityEngine.XR.ARFoundation.ARHumanBodiesChangedEventArgs)
extern void HumanBodyTrackerWithOptions_OnHumanBodiesChanged_m4C32543F85D6D412DFB35B349291C53CF74F664D (void);
// 0x00000060 System.Void HumanBodyTrackerWithOptions::.ctor()
extern void HumanBodyTrackerWithOptions__ctor_m666C6025DE2C2A9D9176003904E7115993299A0C (void);
// 0x00000061 UnityEngine.Transform HumanBoneController::get_skeletonRoot()
extern void HumanBoneController_get_skeletonRoot_m2E43D2A2F0706D8856B90C9C99789E5691282446 (void);
// 0x00000062 System.Void HumanBoneController::set_skeletonRoot(UnityEngine.Transform)
extern void HumanBoneController_set_skeletonRoot_m13A3FC33288A924ABBA257B646CF94C09CFA8638 (void);
// 0x00000063 System.Void HumanBoneController::InitializeSkeletonJoints()
extern void HumanBoneController_InitializeSkeletonJoints_m2B1B50242F4CA38FE2B319782D15A097F27BAD4A (void);
// 0x00000064 System.Void HumanBoneController::ApplyBodyPose(UnityEngine.XR.ARFoundation.ARHumanBody,UnityEngine.Vector3)
extern void HumanBoneController_ApplyBodyPose_m315669C85868C33166429DBFE0F567F81784D574 (void);
// 0x00000065 System.Void HumanBoneController::ProcessJoint(UnityEngine.Transform)
extern void HumanBoneController_ProcessJoint_m390BA91B605E4D8B3FBBA5C95C2A56212EB6D7B8 (void);
// 0x00000066 System.Int32 HumanBoneController::GetJointIndex(System.String)
extern void HumanBoneController_GetJointIndex_mA7DEE8361A4BA73172924F6E7B88B2410D427147 (void);
// 0x00000067 System.Void HumanBoneController::.ctor()
extern void HumanBoneController__ctor_m47A6C770166CBE9881ABEE5B42B0B8C9A2EF3426 (void);
// 0x00000068 System.Void LightEstimation::Awake()
extern void LightEstimation_Awake_m810C5DDEF38182C155ED925189FCEE5A6156ED0F (void);
// 0x00000069 System.Void LightEstimation::OnEnable()
extern void LightEstimation_OnEnable_m14852BF0988F64CA2BD06CAF46F3F01E0E4ABD8C (void);
// 0x0000006A System.Void LightEstimation::OnDisable()
extern void LightEstimation_OnDisable_mB4EE490FD2CE5C671376ED02803EEEC99DCB6941 (void);
// 0x0000006B System.Void LightEstimation::FrameUpdated(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void LightEstimation_FrameUpdated_m7C9F3BF723144C9616A7FA38D0BB6DAFD9734611 (void);
// 0x0000006C System.Void LightEstimation::.ctor()
extern void LightEstimation__ctor_m1D22C524A3F3BACE47DAD18291C9A7265A768E14 (void);
// 0x0000006D System.Void MeasurementController::Awake()
extern void MeasurementController_Awake_m444D2F091D689C081AE2D2F9BC5FA255CDE5AA20 (void);
// 0x0000006E System.Void MeasurementController::Dismiss()
extern void MeasurementController_Dismiss_mCD666A5BD23DDE32FB5E6FA1CD26A959DCD24D82 (void);
// 0x0000006F System.Void MeasurementController::OnEnable()
extern void MeasurementController_OnEnable_mE9989E2EE6BC7EA910ED6738FA3CA8558A2E284B (void);
// 0x00000070 System.Void MeasurementController::Update()
extern void MeasurementController_Update_mFDD80DE1273E9C02877CB8E4FDBADF2B2EC46A37 (void);
// 0x00000071 System.Void MeasurementController::.ctor()
extern void MeasurementController__ctor_m7B2E2FA3D26390CFCF1CDC9A32995FF2C8AD4D31 (void);
// 0x00000072 System.Void MeasurementController::.cctor()
extern void MeasurementController__cctor_mE9B8844F7825E1ABFA54A510E12A613AAC87C3B6 (void);
// 0x00000073 System.Void PeopleOcclusion::Awake()
extern void PeopleOcclusion_Awake_m7FA9AA2785D3A88870DB718AAB021F8B06F9D429 (void);
// 0x00000074 System.Void PeopleOcclusion::OnEnable()
extern void PeopleOcclusion_OnEnable_mEA4D53CB6B4024E91075271115B46795D28B2D6C (void);
// 0x00000075 System.Void PeopleOcclusion::OnDisable()
extern void PeopleOcclusion_OnDisable_mFF732BF3E33EE6E56A69F8F0589795CCBBD03513 (void);
// 0x00000076 System.Void PeopleOcclusion::NextTexture()
extern void PeopleOcclusion_NextTexture_mBA0EAD6F5E39CF668CDC27D1B60154EC32084176 (void);
// 0x00000077 System.Void PeopleOcclusion::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void PeopleOcclusion_OnRenderImage_mFBD534D38488FEBA50018D8C02941849919C51FB (void);
// 0x00000078 System.Void PeopleOcclusion::OnCameraFrameReceived(UnityEngine.XR.ARFoundation.ARCameraFrameEventArgs)
extern void PeopleOcclusion_OnCameraFrameReceived_m1046CE15B18AAB56DA3C22D485549F474FDFC8F1 (void);
// 0x00000079 System.Boolean PeopleOcclusion::PeopleOcclusionSupported()
extern void PeopleOcclusion_PeopleOcclusionSupported_mF663A01F17DEEFCF3ED3261252ACFE98D574CC2C (void);
// 0x0000007A System.Void PeopleOcclusion::RefreshCameraFeedTexture()
extern void PeopleOcclusion_RefreshCameraFeedTexture_mB0DB6C9AF91FBE4104BCD8EFF258D89BA0718BC8 (void);
// 0x0000007B System.Single PeopleOcclusion::CalculateUVMultiplierLandScape(UnityEngine.Texture2D)
extern void PeopleOcclusion_CalculateUVMultiplierLandScape_m1BD9B2CBA7DB653CA5E85F602436DCEC600C0A20 (void);
// 0x0000007C System.Single PeopleOcclusion::CalculateUVMultiplierPortrait(UnityEngine.Texture2D)
extern void PeopleOcclusion_CalculateUVMultiplierPortrait_mB89FE071BE30CC5C50F504942EA7C75E2853923C (void);
// 0x0000007D System.Void PeopleOcclusion::.ctor()
extern void PeopleOcclusion__ctor_m34BD43B16F3F109904584AC9B350B17A7E9F63BC (void);
// 0x0000007E System.Void PlacementAndLaunchingCanvasController::Awake()
extern void PlacementAndLaunchingCanvasController_Awake_mD72433C784D34A7E17B48864195150B1380091A2 (void);
// 0x0000007F System.Void PlacementAndLaunchingCanvasController::Dismiss()
extern void PlacementAndLaunchingCanvasController_Dismiss_mE7579FB05A5BE220A4B83A408C78D998809D0EB9 (void);
// 0x00000080 System.Void PlacementAndLaunchingCanvasController::Update()
extern void PlacementAndLaunchingCanvasController_Update_mEFAA0C6BE1B01AF4F3DDC07A53115E2FABF9EB21 (void);
// 0x00000081 System.Void PlacementAndLaunchingCanvasController::.ctor()
extern void PlacementAndLaunchingCanvasController__ctor_m3037B08D61479108650A400838A6BDA4F00F3791 (void);
// 0x00000082 System.Void PlacementBoundingArea::Awake()
extern void PlacementBoundingArea_Awake_m706BEE7DFA693F3CCEEBC9C6731D35378CB07D76 (void);
// 0x00000083 System.Void PlacementBoundingArea::SetupBounds()
extern void PlacementBoundingArea_SetupBounds_m0CF087F85054870F897DEAF345EE82D7B1CB282C (void);
// 0x00000084 System.Void PlacementBoundingArea::Update()
extern void PlacementBoundingArea_Update_m5853DBECBE1E7F64D5A0D7FC24C01C980E13A531 (void);
// 0x00000085 System.Void PlacementBoundingArea::DrawBoundingArea(System.Boolean)
extern void PlacementBoundingArea_DrawBoundingArea_mBC02EC9743D87469D0B307E119F27A20D62135E2 (void);
// 0x00000086 System.Void PlacementBoundingArea::.ctor()
extern void PlacementBoundingArea__ctor_mADF3B3A3A174BF960209334C94C93EBD76764577 (void);
// 0x00000087 UnityEngine.GameObject PlacementController::get_PlacedPrefab()
extern void PlacementController_get_PlacedPrefab_m7D6746941CEC265D743AE29DC8013F678222C357 (void);
// 0x00000088 System.Void PlacementController::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementController_set_PlacedPrefab_m637D984CBB79A47B292E9C20592E47CF57A00C9F (void);
// 0x00000089 System.Void PlacementController::Awake()
extern void PlacementController_Awake_m6D3B9373E51E7E9778AF3F89C9F94F0095144D2E (void);
// 0x0000008A System.Boolean PlacementController::TryGetTouchPosition(UnityEngine.Vector2&)
extern void PlacementController_TryGetTouchPosition_mAEA65EDBD7E6B64C2C6EAE2E3B3FC862BC5F222A (void);
// 0x0000008B System.Void PlacementController::Update()
extern void PlacementController_Update_m195CE59EBD6660E533FE832C5BCF0F328A09F7F8 (void);
// 0x0000008C System.Void PlacementController::.ctor()
extern void PlacementController__ctor_m05CF4708C82DFE821DD99261AED3B0C9E4FC0F4C (void);
// 0x0000008D System.Void PlacementController::.cctor()
extern void PlacementController__cctor_m13B32164C0AF1168E34CFBBF8E6E24AE2AC9576B (void);
// 0x0000008E System.Void PlacementControllerWithMultiple::Awake()
extern void PlacementControllerWithMultiple_Awake_m8AB6E254F9FC049E9F1EE6E3F09DC68AE30CDB5B (void);
// 0x0000008F System.Void PlacementControllerWithMultiple::Dismiss()
extern void PlacementControllerWithMultiple_Dismiss_mC7A66D6D7C629CB35CB38C558420F5B985E69C94 (void);
// 0x00000090 System.Void PlacementControllerWithMultiple::ChangePrefabTo(System.String)
extern void PlacementControllerWithMultiple_ChangePrefabTo_m5A6815CA78F6834EB2D46EA996883FBC36AA2EB7 (void);
// 0x00000091 System.Boolean PlacementControllerWithMultiple::TryGetTouchPosition(UnityEngine.Vector2&)
extern void PlacementControllerWithMultiple_TryGetTouchPosition_m5B4B45EC5291F118EFBEFC8C12B41A17339DDFED (void);
// 0x00000092 System.Void PlacementControllerWithMultiple::Update()
extern void PlacementControllerWithMultiple_Update_m7E5F0EB395ADDB25F4BB83105A5A904392B46447 (void);
// 0x00000093 System.Void PlacementControllerWithMultiple::.ctor()
extern void PlacementControllerWithMultiple__ctor_mDCB427B596983DFB0991E83529F0CFF1ABA6DAB7 (void);
// 0x00000094 System.Void PlacementControllerWithMultiple::.cctor()
extern void PlacementControllerWithMultiple__cctor_m79C8DB39D492570076D62E0555FADEACC26DCE17 (void);
// 0x00000095 System.Void PlacementControllerWithMultiple::<Awake>b__8_0()
extern void PlacementControllerWithMultiple_U3CAwakeU3Eb__8_0_m47DDED8B565FDC0872465DCFA8A435008B2A3CC2 (void);
// 0x00000096 System.Void PlacementControllerWithMultiple::<Awake>b__8_1()
extern void PlacementControllerWithMultiple_U3CAwakeU3Eb__8_1_m3ACEE2609A4F12865D9B22F2785D7CF1DB3581DE (void);
// 0x00000097 System.Void PlacementControllerWithMultiple::<Awake>b__8_2()
extern void PlacementControllerWithMultiple_U3CAwakeU3Eb__8_2_m781EF4589495177B662FAE7DCF47CC1F5550AF31 (void);
// 0x00000098 UnityEngine.GameObject PlacementEnableAndDisable::get_PlacedPrefab()
extern void PlacementEnableAndDisable_get_PlacedPrefab_m77D62F1690E06ED64DFDC9DF92BE62DC0850BD7D (void);
// 0x00000099 System.Void PlacementEnableAndDisable::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementEnableAndDisable_set_PlacedPrefab_m45010BFD529A569D3BAE363935524882B63DA4A2 (void);
// 0x0000009A System.Void PlacementEnableAndDisable::Awake()
extern void PlacementEnableAndDisable_Awake_m95F7683B691BED72BDD0FD13506ECFABCCF27329 (void);
// 0x0000009B System.Void PlacementEnableAndDisable::TogglePlaneDetection()
extern void PlacementEnableAndDisable_TogglePlaneDetection_mBA84BEA003C774CF7D10B86A649747DDA836A826 (void);
// 0x0000009C System.Void PlacementEnableAndDisable::ChangePrefabSelection(System.String)
extern void PlacementEnableAndDisable_ChangePrefabSelection_m5BDF2A46CFB992A42D25EBD98147AC8A0A974297 (void);
// 0x0000009D System.Void PlacementEnableAndDisable::Dismiss()
extern void PlacementEnableAndDisable_Dismiss_m4F987C404DE2647C1D68234526B7DDD40AC24037 (void);
// 0x0000009E System.Void PlacementEnableAndDisable::Update()
extern void PlacementEnableAndDisable_Update_mAEA59A079B26B40F3BC7A730C509740FCB8F4AB6 (void);
// 0x0000009F System.Void PlacementEnableAndDisable::.ctor()
extern void PlacementEnableAndDisable__ctor_m367DAE2BCCC2CB00F8E8445E900465D49244848E (void);
// 0x000000A0 System.Void PlacementEnableAndDisable::.cctor()
extern void PlacementEnableAndDisable__cctor_m516BB221E7F1F59E63A238DACD388C0F2A6C5C2D (void);
// 0x000000A1 System.Void PlacementEnableAndDisable::<Awake>b__18_0()
extern void PlacementEnableAndDisable_U3CAwakeU3Eb__18_0_mBC48A5D3E233CADCAEFE7744E55DE3C2D5A2FCF3 (void);
// 0x000000A2 System.Void PlacementEnableAndDisable::<Awake>b__18_1()
extern void PlacementEnableAndDisable_U3CAwakeU3Eb__18_1_m5814B3B83D3414804285C1D3E0F9774793AABEF8 (void);
// 0x000000A3 System.Void PlacementEnableAndDisable::<Awake>b__18_2()
extern void PlacementEnableAndDisable_U3CAwakeU3Eb__18_2_mED953F4CDED930B9AB5B109054C5925349CAD9EE (void);
// 0x000000A4 System.Boolean PlacementObject::get_Selected()
extern void PlacementObject_get_Selected_mA798C2D2BF95A80BBADBB8315E701A78E612ADEB (void);
// 0x000000A5 System.Void PlacementObject::set_Selected(System.Boolean)
extern void PlacementObject_set_Selected_m45896B6FE7BA5BABBED21C2EC5A142C97BF98697 (void);
// 0x000000A6 System.Boolean PlacementObject::get_Locked()
extern void PlacementObject_get_Locked_m32A3EB23EF8D75B807062D86FBDF7F83EB40A48B (void);
// 0x000000A7 System.Void PlacementObject::set_Locked(System.Boolean)
extern void PlacementObject_set_Locked_mD8B284B7723E7CA333CBDA6C198A7DB793F3BE17 (void);
// 0x000000A8 System.Void PlacementObject::SetOverlayText(System.String)
extern void PlacementObject_SetOverlayText_m46C49B68F6237B68F7F5B58603B08096E37ADCA5 (void);
// 0x000000A9 System.Void PlacementObject::Awake()
extern void PlacementObject_Awake_m2D81C5E6930C59846362DAF863A39AC90B24D15C (void);
// 0x000000AA System.Void PlacementObject::ToggleOverlay()
extern void PlacementObject_ToggleOverlay_m30DAA7869ECAC96E6751844C6974FD35AED55C11 (void);
// 0x000000AB System.Void PlacementObject::ToggleCanvas()
extern void PlacementObject_ToggleCanvas_mA9B9B37C562321A9E9706AF5FB06C4F14ADBD17A (void);
// 0x000000AC System.Void PlacementObject::.ctor()
extern void PlacementObject__ctor_m39577B21713D7D5F4502865A4C4024D35C7212CC (void);
// 0x000000AD System.Void PlacementRotation::Awake()
extern void PlacementRotation_Awake_mE77D4B707C0A5376AA455CD3D36BF5C108380257 (void);
// 0x000000AE System.Void PlacementRotation::Update()
extern void PlacementRotation_Update_mEFF34A6031D4CE73E2794BA665A62E15BAFB9010 (void);
// 0x000000AF System.Void PlacementRotation::.ctor()
extern void PlacementRotation__ctor_m810455AACD54490C47BAB24621C981509290F541 (void);
// 0x000000B0 System.Void PlacementWithDraggingDroppingController::Awake()
extern void PlacementWithDraggingDroppingController_Awake_m94769FE3B6350B058977089203E1CC49174CF5B8 (void);
// 0x000000B1 System.Void PlacementWithDraggingDroppingController::Dismiss()
extern void PlacementWithDraggingDroppingController_Dismiss_m8B27CBA48217084982978776C4530B5AA8A732A7 (void);
// 0x000000B2 System.Void PlacementWithDraggingDroppingController::Lock()
extern void PlacementWithDraggingDroppingController_Lock_mA6F7FACA1D39F4F9AE5EE09AB4BF4DA26BD3A7F5 (void);
// 0x000000B3 System.Void PlacementWithDraggingDroppingController::Update()
extern void PlacementWithDraggingDroppingController_Update_m40F91837A23F99F6B822103B5960A463A8F86FB2 (void);
// 0x000000B4 System.Void PlacementWithDraggingDroppingController::.ctor()
extern void PlacementWithDraggingDroppingController__ctor_m998CCA8F66D84DC2700D274DD5BF3B5C3FB86409 (void);
// 0x000000B5 System.Void PlacementWithDraggingDroppingController::.cctor()
extern void PlacementWithDraggingDroppingController__cctor_m661DE9D762D8A6E80205057AC15498A854E083A4 (void);
// 0x000000B6 UnityEngine.GameObject[] PlacementWithManyController::get_PlacedPrefab()
extern void PlacementWithManyController_get_PlacedPrefab_m64312779DF8AB7D0AF2477ED6ED9CC467C020C60 (void);
// 0x000000B7 System.Void PlacementWithManyController::set_PlacedPrefab(UnityEngine.GameObject[])
extern void PlacementWithManyController_set_PlacedPrefab_m11A7C071E8E481A49C67DA879DC37EF425D2EDB4 (void);
// 0x000000B8 System.Void PlacementWithManyController::Awake()
extern void PlacementWithManyController_Awake_m36755D3C13265428DB2B076AA2F8BE2FECC51E3F (void);
// 0x000000B9 System.Void PlacementWithManyController::Dismiss()
extern void PlacementWithManyController_Dismiss_m6683B912D4D274010C518C82530E7008941380C7 (void);
// 0x000000BA System.Void PlacementWithManyController::Update()
extern void PlacementWithManyController_Update_mC4CED3977271C1C19D1809D579DB55B20283ECE2 (void);
// 0x000000BB System.Void PlacementWithManyController::.ctor()
extern void PlacementWithManyController__ctor_m72B73173047BF7921D520FAD3167568FDA9549C3 (void);
// 0x000000BC System.Void PlacementWithManyController::.cctor()
extern void PlacementWithManyController__cctor_m8392C0A431A8DF479E6EFCE10FD9BD89697A8A7F (void);
// 0x000000BD System.Void PlacementWithManyObjectsController::Awake()
extern void PlacementWithManyObjectsController_Awake_mAC420E6EB17B9AD6E7D6F0B4BE388A2F97685FF8 (void);
// 0x000000BE System.Void PlacementWithManyObjectsController::Dismiss()
extern void PlacementWithManyObjectsController_Dismiss_m7E771D455CA010E31A99C4730B90A5055D30CF0D (void);
// 0x000000BF System.Void PlacementWithManyObjectsController::Update()
extern void PlacementWithManyObjectsController_Update_m7B53CD7FCA505CA83C5D5DB0921F0EA977D4E21A (void);
// 0x000000C0 System.Void PlacementWithManyObjectsController::.ctor()
extern void PlacementWithManyObjectsController__ctor_m855484C27F13842C6DBAB591013CCC41DCB6F86C (void);
// 0x000000C1 System.Void PlacementWithManyObjectsController::.cctor()
extern void PlacementWithManyObjectsController__cctor_m728ED2A1F6780E44B661CD8D221EF2FD3259195D (void);
// 0x000000C2 System.Void PlacementWithManySelectionController::Awake()
extern void PlacementWithManySelectionController_Awake_m7FB46F58056E1D329CC7BCB2CC73BEE992F7F189 (void);
// 0x000000C3 System.Void PlacementWithManySelectionController::Start()
extern void PlacementWithManySelectionController_Start_m6A46DD8E87A26FC70D3B85E47975D4FE20D887B9 (void);
// 0x000000C4 System.Void PlacementWithManySelectionController::Dismiss()
extern void PlacementWithManySelectionController_Dismiss_m848938B2B202E58604F8647061450E3EA8C53771 (void);
// 0x000000C5 System.Void PlacementWithManySelectionController::Update()
extern void PlacementWithManySelectionController_Update_m2B7944FC1D933F85AFD18957932C8C9C30B5FB8F (void);
// 0x000000C6 System.Void PlacementWithManySelectionController::ChangeSelectedObject(PlacementObject)
extern void PlacementWithManySelectionController_ChangeSelectedObject_m880A8FCA5020393D6E01536D1DF5C19FA01EFD32 (void);
// 0x000000C7 System.Void PlacementWithManySelectionController::.ctor()
extern void PlacementWithManySelectionController__ctor_m5F4AF98465F91D60DFB003B45C88A673250DDAD4 (void);
// 0x000000C8 UnityEngine.GameObject PlacementWithManySelectionWithScaleController::get_PlacedPrefab()
extern void PlacementWithManySelectionWithScaleController_get_PlacedPrefab_m15D353C84B54ED5E347B9DBBE08F0105C9AD24BC (void);
// 0x000000C9 System.Void PlacementWithManySelectionWithScaleController::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementWithManySelectionWithScaleController_set_PlacedPrefab_mB2C8AD3A2AF5C83B95B0ECC1082F8A6F5523BE92 (void);
// 0x000000CA System.Void PlacementWithManySelectionWithScaleController::Awake()
extern void PlacementWithManySelectionWithScaleController_Awake_mD2008B3350230B99FB0E51CC4D1953D1651C7E4F (void);
// 0x000000CB System.Void PlacementWithManySelectionWithScaleController::ToggleOptions()
extern void PlacementWithManySelectionWithScaleController_ToggleOptions_m664C1D3813269AEC288E60CA6755656EFCC0F596 (void);
// 0x000000CC System.Void PlacementWithManySelectionWithScaleController::Dismiss()
extern void PlacementWithManySelectionWithScaleController_Dismiss_m880426D5AABF5F88E63DC69AA495744CCA794BCA (void);
// 0x000000CD System.Void PlacementWithManySelectionWithScaleController::ScaleChanged(System.Single)
extern void PlacementWithManySelectionWithScaleController_ScaleChanged_m09B54B0D8CD6802BC9D8EC503CF0FB0AB7CD79BE (void);
// 0x000000CE System.Void PlacementWithManySelectionWithScaleController::Update()
extern void PlacementWithManySelectionWithScaleController_Update_mF3589DEE8158AF9C5585A1A0040DB5A594920DCE (void);
// 0x000000CF System.Void PlacementWithManySelectionWithScaleController::.ctor()
extern void PlacementWithManySelectionWithScaleController__ctor_m7FAD25C506E5770F71456CEBF58FA2581777AE7E (void);
// 0x000000D0 System.Void PlacementWithManySelectionWithScaleController::.cctor()
extern void PlacementWithManySelectionWithScaleController__cctor_m3B90CBCDB02B89E46570C9AAF73951982A9B6262 (void);
// 0x000000D1 UnityEngine.GameObject PlacementWithManySinglePrefabSelectionController::get_PlacedPrefab()
extern void PlacementWithManySinglePrefabSelectionController_get_PlacedPrefab_mB3E6EE1F3C34F436A223BD8836DC2C5FFCF10D16 (void);
// 0x000000D2 System.Void PlacementWithManySinglePrefabSelectionController::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementWithManySinglePrefabSelectionController_set_PlacedPrefab_m70B9EBFB2AAA2AF88E3F40CAD7364D261A6EAEF6 (void);
// 0x000000D3 System.Void PlacementWithManySinglePrefabSelectionController::Awake()
extern void PlacementWithManySinglePrefabSelectionController_Awake_m7F4ADEC2626E71E58B892538B25E7601D84A51D1 (void);
// 0x000000D4 System.Void PlacementWithManySinglePrefabSelectionController::Dismiss()
extern void PlacementWithManySinglePrefabSelectionController_Dismiss_m7CB7156BC1C41F26B9E73B9AD5D3A02AE83BD1C8 (void);
// 0x000000D5 System.Void PlacementWithManySinglePrefabSelectionController::Update()
extern void PlacementWithManySinglePrefabSelectionController_Update_m4F03734D37CD1306EC8A7012B2E90B1EA390566D (void);
// 0x000000D6 System.Void PlacementWithManySinglePrefabSelectionController::.ctor()
extern void PlacementWithManySinglePrefabSelectionController__ctor_m986C0913F19316F9A03058449D865401C592D603 (void);
// 0x000000D7 System.Void PlacementWithManySinglePrefabSelectionController::.cctor()
extern void PlacementWithManySinglePrefabSelectionController__cctor_m694A1B31C3A14E629704090B495A45CD0EAE9AF2 (void);
// 0x000000D8 UnityEngine.GameObject PlacementWithMultipleDraggingDroppingController::get_PlacedPrefab()
extern void PlacementWithMultipleDraggingDroppingController_get_PlacedPrefab_m2317A7C4B67054A82F567A44033C608620774B63 (void);
// 0x000000D9 System.Void PlacementWithMultipleDraggingDroppingController::set_PlacedPrefab(UnityEngine.GameObject)
extern void PlacementWithMultipleDraggingDroppingController_set_PlacedPrefab_m35573B9B9C397F1495063452AB3513FFB8186289 (void);
// 0x000000DA System.Void PlacementWithMultipleDraggingDroppingController::Awake()
extern void PlacementWithMultipleDraggingDroppingController_Awake_m32EEAF62A803B2B80C3F083359C7F4E3E3FCE948 (void);
// 0x000000DB System.Void PlacementWithMultipleDraggingDroppingController::ChangePrefabSelection(System.String)
extern void PlacementWithMultipleDraggingDroppingController_ChangePrefabSelection_mD8663D76FC662C424A99C93BE2F37D52C1A1FDE5 (void);
// 0x000000DC System.Void PlacementWithMultipleDraggingDroppingController::Dismiss()
extern void PlacementWithMultipleDraggingDroppingController_Dismiss_m74A55FEB11266B43F819164E9BE0027DD9C1A22E (void);
// 0x000000DD System.Void PlacementWithMultipleDraggingDroppingController::Update()
extern void PlacementWithMultipleDraggingDroppingController_Update_m0FE9FD6D19EA032A8B476ACB95CA43C1C89BB0C9 (void);
// 0x000000DE System.Void PlacementWithMultipleDraggingDroppingController::.ctor()
extern void PlacementWithMultipleDraggingDroppingController__ctor_m64E3AA08A57BB231C8013CC52307D2C0B2195759 (void);
// 0x000000DF System.Void PlacementWithMultipleDraggingDroppingController::.cctor()
extern void PlacementWithMultipleDraggingDroppingController__cctor_mCA8A673AE84CDC8B440E4B3431CC5D5D6B545BD7 (void);
// 0x000000E0 System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_0()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m7976306C9AD1C7A08A9FF89809C71BE67DB8A8A4 (void);
// 0x000000E1 System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_1()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m53BC05EB8FF55B5C927458A90CDE8E090B877094 (void);
// 0x000000E2 System.Void PlacementWithMultipleDraggingDroppingController::<Awake>b__16_2()
extern void PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m37D674BD4783C0C61CE485C6B4121DADCEADB615 (void);
// 0x000000E3 System.Void ReferencePointManager::Awake()
extern void ReferencePointManager_Awake_mC60B2B8B1E2CF0FF5C44D32E5DD8EDE5C4CE21B1 (void);
// 0x000000E4 System.Void ReferencePointManager::Update()
extern void ReferencePointManager_Update_mDE36DD75684902F71C2A888EFBD7105DCF76AE85 (void);
// 0x000000E5 System.Void ReferencePointManager::TogglePlaneDetection()
extern void ReferencePointManager_TogglePlaneDetection_m1830388CE1571D8DC4F353812933D5174D6DBD93 (void);
// 0x000000E6 System.Void ReferencePointManager::ClearReferencePoints()
extern void ReferencePointManager_ClearReferencePoints_m215DB023849FB84311297EB2056AF9A4456BC4EE (void);
// 0x000000E7 System.Void ReferencePointManager::.ctor()
extern void ReferencePointManager__ctor_m755FCB9492698121064B9774242BA587549003DA (void);
// 0x000000E8 System.Void ReferencePointManager::.cctor()
extern void ReferencePointManager__cctor_m3F9F7C5DC360D4EAE489ACDFCBFEF0A9CD84C483 (void);
// 0x000000E9 System.Void ReferencePointManagerWithCameraDistance::Awake()
extern void ReferencePointManagerWithCameraDistance_Awake_m9BB036D0FE2A0547B21EFEA42BC2D36025A8344F (void);
// 0x000000EA System.Void ReferencePointManagerWithCameraDistance::Update()
extern void ReferencePointManagerWithCameraDistance_Update_m6B8E7E7E76B1F9EDC14129B84C52968E093C5992 (void);
// 0x000000EB System.Void ReferencePointManagerWithCameraDistance::ClearReferencePoints()
extern void ReferencePointManagerWithCameraDistance_ClearReferencePoints_m182123D304A87D379A8F19C08635CA0BD8AFDA50 (void);
// 0x000000EC System.Void ReferencePointManagerWithCameraDistance::.ctor()
extern void ReferencePointManagerWithCameraDistance__ctor_mD9AF232E9DFFD733A9BAC6BCA7184D2523683A16 (void);
// 0x000000ED System.Void ReferencePointManagerWithFeaturePoints::Awake()
extern void ReferencePointManagerWithFeaturePoints_Awake_mB375336F2234CD91C996EFAB1A13727C7A828937 (void);
// 0x000000EE System.Void ReferencePointManagerWithFeaturePoints::Update()
extern void ReferencePointManagerWithFeaturePoints_Update_mCABF123DC8B9FC12D9451004F47AE076A008EE06 (void);
// 0x000000EF System.Void ReferencePointManagerWithFeaturePoints::ToggleDetection()
extern void ReferencePointManagerWithFeaturePoints_ToggleDetection_mD1448A8A43FF6EB9C46244953AECECDAE618DAA0 (void);
// 0x000000F0 System.Void ReferencePointManagerWithFeaturePoints::ClearReferencePoints()
extern void ReferencePointManagerWithFeaturePoints_ClearReferencePoints_mB04EC353222171D2388E91139C3788635A442EE2 (void);
// 0x000000F1 System.Void ReferencePointManagerWithFeaturePoints::.ctor()
extern void ReferencePointManagerWithFeaturePoints__ctor_m39879B98823F456CB185B24558B6BEA078958D1C (void);
// 0x000000F2 System.Void ReferencePointManagerWithFeaturePoints::.cctor()
extern void ReferencePointManagerWithFeaturePoints__cctor_mCAC5552969771A57CA1DBDFC1E9D4705123C41D1 (void);
// 0x000000F3 UnityEngine.Camera ScreenSpaceJointVisualizer::get_arCamera()
extern void ScreenSpaceJointVisualizer_get_arCamera_m128BEF2E57E20E4427EE9826A916A1B087492BBD (void);
// 0x000000F4 System.Void ScreenSpaceJointVisualizer::set_arCamera(UnityEngine.Camera)
extern void ScreenSpaceJointVisualizer_set_arCamera_m6BC0FB3BE4A119B6AFE9704BF4BE2C367149334D (void);
// 0x000000F5 UnityEngine.XR.ARFoundation.ARHumanBodyManager ScreenSpaceJointVisualizer::get_humanBodyManager()
extern void ScreenSpaceJointVisualizer_get_humanBodyManager_m734494DABEDD13584C84EA0E68D99081E224DD54 (void);
// 0x000000F6 System.Void ScreenSpaceJointVisualizer::set_humanBodyManager(UnityEngine.XR.ARFoundation.ARHumanBodyManager)
extern void ScreenSpaceJointVisualizer_set_humanBodyManager_mA323AE1C7FBC486559B35DD43B9048A50640E2F7 (void);
// 0x000000F7 UnityEngine.GameObject ScreenSpaceJointVisualizer::get_lineRendererPrefab()
extern void ScreenSpaceJointVisualizer_get_lineRendererPrefab_m10E2D0BD900B08385DA937E593F76052D0D1FFF8 (void);
// 0x000000F8 System.Void ScreenSpaceJointVisualizer::set_lineRendererPrefab(UnityEngine.GameObject)
extern void ScreenSpaceJointVisualizer_set_lineRendererPrefab_m8E4678D5F42488BD9B47C55934469B345DA67775 (void);
// 0x000000F9 System.Void ScreenSpaceJointVisualizer::Awake()
extern void ScreenSpaceJointVisualizer_Awake_m28374A2BEC6437A3BF5DACF9312B519252179EB7 (void);
// 0x000000FA System.Void ScreenSpaceJointVisualizer::UpdateRenderer(Unity.Collections.NativeArray`1<UnityEngine.XR.ARSubsystems.XRHumanBodyPose2DJoint>,System.Int32)
extern void ScreenSpaceJointVisualizer_UpdateRenderer_m9F1206B04515C4744A44EE9B44E7203F21A1AE96 (void);
// 0x000000FB System.Void ScreenSpaceJointVisualizer::Update()
extern void ScreenSpaceJointVisualizer_Update_mEC032154B52D456A88863E838CC7784D7CD5F59D (void);
// 0x000000FC System.Void ScreenSpaceJointVisualizer::HideJointLines()
extern void ScreenSpaceJointVisualizer_HideJointLines_mED607BF3196A13BF2B1FE5BCDAE6F44E2013E592 (void);
// 0x000000FD System.Void ScreenSpaceJointVisualizer::.ctor()
extern void ScreenSpaceJointVisualizer__ctor_m20024FC7F92DC5B513D5D2A9CD95168B13227E43 (void);
// 0x000000FE System.Void ScreenSpaceJointVisualizer::.cctor()
extern void ScreenSpaceJointVisualizer__cctor_mC44F836911817EE0D1F4CBCD7713879DFD29F783 (void);
// 0x000000FF System.Void SelectionFromCameraOrigin::Awake()
extern void SelectionFromCameraOrigin_Awake_m12D4829DC922B9AC29423451F66B6A46D389576E (void);
// 0x00000100 System.Void SelectionFromCameraOrigin::Start()
extern void SelectionFromCameraOrigin_Start_mF103AAAB91D6B6BCCCD1DE0C02FDF83E03F86918 (void);
// 0x00000101 System.Void SelectionFromCameraOrigin::Dismiss()
extern void SelectionFromCameraOrigin_Dismiss_m2CB361B419646011DFC5F785AF8FFECBDCAF0143 (void);
// 0x00000102 System.Void SelectionFromCameraOrigin::Update()
extern void SelectionFromCameraOrigin_Update_m8E8133D652FE36580E52197663CDC6BB6FF9CB16 (void);
// 0x00000103 System.Void SelectionFromCameraOrigin::ChangeSelectedObject(PlacementObject)
extern void SelectionFromCameraOrigin_ChangeSelectedObject_m73400F6198BC67FD04CFC60EAF34FDE3A43115B9 (void);
// 0x00000104 System.Void SelectionFromCameraOrigin::.ctor()
extern void SelectionFromCameraOrigin__ctor_mCE14DB4DC5D8B285A0D47C6DBB97D7E2242626B9 (void);
// 0x00000105 System.Void SimpleUI::Awake()
extern void SimpleUI_Awake_mA08B2FA6E73CAF74D315F82D99E989F17C0D759A (void);
// 0x00000106 System.Void SimpleUI::Dismiss()
extern void SimpleUI_Dismiss_mC78EF091D89E53ADF02AFA6E9D21631F073A15EF (void);
// 0x00000107 System.Void SimpleUI::.ctor()
extern void SimpleUI__ctor_m510966DDE395DEA4E64FB8797E8AFC0ADF068650 (void);
// 0x00000108 System.Boolean SpawnSuperPowers::get_Started()
extern void SpawnSuperPowers_get_Started_mDE334338F6962B73EB45427507B2BB9CE9DF1156 (void);
// 0x00000109 System.Void SpawnSuperPowers::set_Started(System.Boolean)
extern void SpawnSuperPowers_set_Started_m6620955416CA9DE4ADE3E996DFD5D07D35C42A7D (void);
// 0x0000010A System.Void SpawnSuperPowers::FixedUpdate()
extern void SpawnSuperPowers_FixedUpdate_m930A8924E396F1CBEE75A2A8A32F2CB16235B139 (void);
// 0x0000010B System.Void SpawnSuperPowers::ApplyForce(UnityEngine.GameObject)
extern void SpawnSuperPowers_ApplyForce_m8F3A9981FEC5BF70B01C52F4B07C2AE6DD0ED566 (void);
// 0x0000010C System.Void SpawnSuperPowers::.ctor()
extern void SpawnSuperPowers__ctor_m58E20D5C3A06B7441A18FD09611D8B43582F1C2A (void);
// 0x0000010D System.Void TrackedImageInfoExtendedManager::Awake()
extern void TrackedImageInfoExtendedManager_Awake_m598F7103464F96AA1E3D0282279721D92F05F702 (void);
// 0x0000010E System.Void TrackedImageInfoExtendedManager::OnEnable()
extern void TrackedImageInfoExtendedManager_OnEnable_m37398ECCAC0BC7F91EFF3C8A5D23DD6D065AF41A (void);
// 0x0000010F System.Void TrackedImageInfoExtendedManager::OnDisable()
extern void TrackedImageInfoExtendedManager_OnDisable_mAE5AB0613E353E38FF17A8CA2E6F83F3D552ABC3 (void);
// 0x00000110 System.Void TrackedImageInfoExtendedManager::Dismiss()
extern void TrackedImageInfoExtendedManager_Dismiss_m7FFFA4AF326EBA03F00C77C2ECA769D90383C0D1 (void);
// 0x00000111 System.Void TrackedImageInfoExtendedManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoExtendedManager_OnTrackedImagesChanged_m9B912C4F9073B8409CCC31051A9913BB51300CC3 (void);
// 0x00000112 System.Void TrackedImageInfoExtendedManager::.ctor()
extern void TrackedImageInfoExtendedManager__ctor_m1F61639687AD368FF12822A90AF778AD4C633D15 (void);
// 0x00000113 System.Void TrackedImageInfoManager::Awake()
extern void TrackedImageInfoManager_Awake_mF2314D24BAD4E61E111D4483DD3B7BE0F47D0F34 (void);
// 0x00000114 System.Void TrackedImageInfoManager::OnEnable()
extern void TrackedImageInfoManager_OnEnable_mF5628468167127206B176871AA5B646BEE851B3D (void);
// 0x00000115 System.Void TrackedImageInfoManager::OnDisable()
extern void TrackedImageInfoManager_OnDisable_mBAA28BD3CBAA1EEEADFB9A224E68FC07D072D609 (void);
// 0x00000116 System.Void TrackedImageInfoManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoManager_OnTrackedImagesChanged_m9A6FB567BDFF58A96A14427F0F455AD02D58F6BB (void);
// 0x00000117 System.Void TrackedImageInfoManager::.ctor()
extern void TrackedImageInfoManager__ctor_m3B347DDF375D07B60D343A6A2E533E50CA114AD8 (void);
// 0x00000118 System.Void TrackedImageInfoMultipleManager::Awake()
extern void TrackedImageInfoMultipleManager_Awake_m6006A76CA1F3CD93DC9B0F468D53ADFDDB3931D2 (void);
// 0x00000119 System.Void TrackedImageInfoMultipleManager::OnEnable()
extern void TrackedImageInfoMultipleManager_OnEnable_m1D976A8CA5A72A97E52CC245D1A74A09C5A74FD3 (void);
// 0x0000011A System.Void TrackedImageInfoMultipleManager::OnDisable()
extern void TrackedImageInfoMultipleManager_OnDisable_mD3934CADCA532033C34C7602EB7A277322A387DC (void);
// 0x0000011B System.Void TrackedImageInfoMultipleManager::Dismiss()
extern void TrackedImageInfoMultipleManager_Dismiss_mD6BCE727BDC6A4FBBB815763A812AAE8E54D82FB (void);
// 0x0000011C System.Void TrackedImageInfoMultipleManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoMultipleManager_OnTrackedImagesChanged_mE6FEB63B99B2FB6B9F120D7108E0F40FD9B6E780 (void);
// 0x0000011D System.Void TrackedImageInfoMultipleManager::UpdateARImage(UnityEngine.XR.ARFoundation.ARTrackedImage)
extern void TrackedImageInfoMultipleManager_UpdateARImage_m83BB6831E4C936032FEFF3A48DF751836D111FC6 (void);
// 0x0000011E System.Void TrackedImageInfoMultipleManager::AssignGameObject(System.String,UnityEngine.Vector3)
extern void TrackedImageInfoMultipleManager_AssignGameObject_mAFE5792C68403222B93CA862A6C3175BDC0754DB (void);
// 0x0000011F System.Void TrackedImageInfoMultipleManager::.ctor()
extern void TrackedImageInfoMultipleManager__ctor_m9E2072969FF1A5EB488216623E429AFA3804FE9C (void);
// 0x00000120 System.Void TrackedImageInfoRuntimeCaptureManager::Start()
extern void TrackedImageInfoRuntimeCaptureManager_Start_m2750A2922CD5F1A6D8367DAF2097A1B2AF0C01C5 (void);
// 0x00000121 System.Collections.IEnumerator TrackedImageInfoRuntimeCaptureManager::CaptureImage()
extern void TrackedImageInfoRuntimeCaptureManager_CaptureImage_m7BE57B8159F2008F80C775FC32FDC7D781E7D622 (void);
// 0x00000122 System.Void TrackedImageInfoRuntimeCaptureManager::ShowTrackerInfo()
extern void TrackedImageInfoRuntimeCaptureManager_ShowTrackerInfo_mB7CEAD73A140C064D3FF86A64F4085A6AB751080 (void);
// 0x00000123 System.Void TrackedImageInfoRuntimeCaptureManager::OnDisable()
extern void TrackedImageInfoRuntimeCaptureManager_OnDisable_m9C96260B90207C430E28A2F64D173B31BA0247AA (void);
// 0x00000124 System.Collections.IEnumerator TrackedImageInfoRuntimeCaptureManager::AddImageJob(UnityEngine.Texture2D)
extern void TrackedImageInfoRuntimeCaptureManager_AddImageJob_m54A9F8A078B420007F3FD2F5AD30BAB47B6FA08A (void);
// 0x00000125 System.Void TrackedImageInfoRuntimeCaptureManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoRuntimeCaptureManager_OnTrackedImagesChanged_m296C47CA2342BAB81E267B166AF814843E18F6FB (void);
// 0x00000126 System.Void TrackedImageInfoRuntimeCaptureManager::.ctor()
extern void TrackedImageInfoRuntimeCaptureManager__ctor_m92A045EBD4A49DA493040BF763F8D72ACDE39CDA (void);
// 0x00000127 System.Void TrackedImageInfoRuntimeCaptureManager::<Start>b__8_0()
extern void TrackedImageInfoRuntimeCaptureManager_U3CStartU3Eb__8_0_m426415C76783782DB61B42594FA640D57FF2BFA8 (void);
// 0x00000128 System.Void TrackedImageInfoRuntimeManager::Start()
extern void TrackedImageInfoRuntimeManager_Start_m573EA728E21437B35DAFA93EE87CE01786E87101 (void);
// 0x00000129 System.Void TrackedImageInfoRuntimeManager::ShowTrackerInfo()
extern void TrackedImageInfoRuntimeManager_ShowTrackerInfo_m2483A8F87CEC50872E543B478FB5456D5AA8BCD2 (void);
// 0x0000012A System.Void TrackedImageInfoRuntimeManager::OnDisable()
extern void TrackedImageInfoRuntimeManager_OnDisable_m5BD533C33D2A10B6FC1EA2EE0D9EB17FB626C88F (void);
// 0x0000012B System.Void TrackedImageInfoRuntimeManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoRuntimeManager_OnTrackedImagesChanged_m76866B5646E66565E95D231159016AEF99E2DDBC (void);
// 0x0000012C System.Void TrackedImageInfoRuntimeManager::.ctor()
extern void TrackedImageInfoRuntimeManager__ctor_mCC91B31B5330089806F5657E5592A80E63C1082D (void);
// 0x0000012D System.Void TrackedImageInfoRuntimeMultipleManager::Start()
extern void TrackedImageInfoRuntimeMultipleManager_Start_m3CBEE333BAF1E38C391D531D202AC3B908503B05 (void);
// 0x0000012E System.Void TrackedImageInfoRuntimeMultipleManager::SetReferenceImageLibrary(System.Int32)
extern void TrackedImageInfoRuntimeMultipleManager_SetReferenceImageLibrary_mFAD6898595E64161184F2CA143F946C7C0CEB121 (void);
// 0x0000012F System.Void TrackedImageInfoRuntimeMultipleManager::ShowTrackerInfo()
extern void TrackedImageInfoRuntimeMultipleManager_ShowTrackerInfo_m4E1686BFDB7A2A2C03222E1C91AB3FFA5CEC7035 (void);
// 0x00000130 System.Void TrackedImageInfoRuntimeMultipleManager::OnDisable()
extern void TrackedImageInfoRuntimeMultipleManager_OnDisable_m4608352878B72933A9B9C3EA76606BAE974C71FF (void);
// 0x00000131 System.Void TrackedImageInfoRuntimeMultipleManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoRuntimeMultipleManager_OnTrackedImagesChanged_m73A812558BD3DE4C8A7B034A53D694EE4F1F5E29 (void);
// 0x00000132 System.Void TrackedImageInfoRuntimeMultipleManager::.ctor()
extern void TrackedImageInfoRuntimeMultipleManager__ctor_mB80780F2B5A0823A1DAE542CF81F443A147A02F0 (void);
// 0x00000133 System.Void TrackedImageInfoRuntimeMultipleManager::<Start>b__9_0()
extern void TrackedImageInfoRuntimeMultipleManager_U3CStartU3Eb__9_0_m73E19B9EE4A09A802DD12B52E3A4FB8EF9B7072A (void);
// 0x00000134 System.Void TrackedImageInfoRuntimeMultipleManager::<Start>b__9_1()
extern void TrackedImageInfoRuntimeMultipleManager_U3CStartU3Eb__9_1_mE648F38B46602FEEB4835AC1DD3814F0DD70E464 (void);
// 0x00000135 System.Void TrackedImageInfoRuntimeSaveManager::Start()
extern void TrackedImageInfoRuntimeSaveManager_Start_m5BCF99CCA0FC231313AD1B89E657A275E054C8B9 (void);
// 0x00000136 System.Void TrackedImageInfoRuntimeSaveManager::ShowTrackerInfo()
extern void TrackedImageInfoRuntimeSaveManager_ShowTrackerInfo_m8E3B04D117A3CB10BC68D758DBF121EA2F916EA3 (void);
// 0x00000137 System.Void TrackedImageInfoRuntimeSaveManager::OnDisable()
extern void TrackedImageInfoRuntimeSaveManager_OnDisable_m3ACE6963B3DD7DFD3AC1BDCE328C38CBFE187F3F (void);
// 0x00000138 System.Collections.IEnumerator TrackedImageInfoRuntimeSaveManager::AddImageJob(UnityEngine.Texture2D)
extern void TrackedImageInfoRuntimeSaveManager_AddImageJob_mC13E2B55FF35D4E2471DF83211DD8D88AF5927C0 (void);
// 0x00000139 System.Void TrackedImageInfoRuntimeSaveManager::OnTrackedImagesChanged(UnityEngine.XR.ARFoundation.ARTrackedImagesChangedEventArgs)
extern void TrackedImageInfoRuntimeSaveManager_OnTrackedImagesChanged_mC5F4554F98F4D49BC09F27C4B8BDCF1641F38F5E (void);
// 0x0000013A System.Void TrackedImageInfoRuntimeSaveManager::.ctor()
extern void TrackedImageInfoRuntimeSaveManager__ctor_m8BC492C71CD67CFFF94C8AED1A362E59FCD8FED8 (void);
// 0x0000013B System.Void TrackedImageInfoRuntimeSaveManager::<Start>b__11_0()
extern void TrackedImageInfoRuntimeSaveManager_U3CStartU3Eb__11_0_mD0A2F285502F85EEDEEAAEE57266D02E53D24241 (void);
// 0x0000013C System.Void TrackedImageInfoRuntimeSaveManager::<Start>b__11_1()
extern void TrackedImageInfoRuntimeSaveManager_U3CStartU3Eb__11_1_mAB6681BC849C22F851D681A7B691AFDDBB8186C7 (void);
// 0x0000013D System.Void AutoTransformDirection::Update()
extern void AutoTransformDirection_Update_mEB9BD2A555569E05A4AA37760724FB94F82C9758 (void);
// 0x0000013E System.Void AutoTransformDirection::.ctor()
extern void AutoTransformDirection__ctor_m5D6D4E92193085DC73F4D0F5709911302F079818 (void);
// 0x0000013F System.Void ClothesController::Start()
extern void ClothesController_Start_mF7E566841469D59949208962E28785839FCE49F8 (void);
// 0x00000140 System.Collections.Generic.List`1<UnityEngine.Sprite> ClothesController::GetSpritesClothes()
extern void ClothesController_GetSpritesClothes_m9185F22E0DC8929CCE1304C49FB958B84EF0E74E (void);
// 0x00000141 System.Void ClothesController::AddToList(System.Collections.Generic.List`1<UnityEngine.Sprite>,System.Int32,Clothes)
extern void ClothesController_AddToList_m4B70988B9E8A54E3A77E6D98339BF74CE2BC25B0 (void);
// 0x00000142 System.Void ClothesController::.ctor()
extern void ClothesController__ctor_mCBE54B7D07721A8A7862C8171F9816B77549D841 (void);
// 0x00000143 System.Void HumanBody::SpawnCloth(UnityEngine.GameObject)
extern void HumanBody_SpawnCloth_mC0262DE6AAA118C5BD2D8AAD33A8CE6A03FD560A (void);
// 0x00000144 System.Void HumanBody::UpdateClothes(System.Collections.Generic.List`1<UnityEngine.Sprite>)
extern void HumanBody_UpdateClothes_mD03E6107E6C41B2040296F9A22CF9ABEFB7253C4 (void);
// 0x00000145 System.Void HumanBody::UpdateClothes(UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite,UnityEngine.Sprite)
extern void HumanBody_UpdateClothes_mA16BAB7D07B9AB691265FEDA783812874835B290 (void);
// 0x00000146 System.Void HumanBody::SetDress(UnityEngine.Sprite)
extern void HumanBody_SetDress_m53D2192885BECC156EF6757F7CC358BAF2111D5D (void);
// 0x00000147 System.Void HumanBody::SetShirt(UnityEngine.Sprite)
extern void HumanBody_SetShirt_mD4BEFFDD712584E1FB5A6F09209834C84AF9E32B (void);
// 0x00000148 System.Void HumanBody::SetHat(UnityEngine.Sprite)
extern void HumanBody_SetHat_mCF228169A979EEF9E214113577DF52B228C7C5A6 (void);
// 0x00000149 System.Void HumanBody::SetTie(UnityEngine.Sprite)
extern void HumanBody_SetTie_m446D7AAFC145AB2974B42417477C52CB1850C4DE (void);
// 0x0000014A System.Void HumanBody::SetBagLeft(UnityEngine.Sprite)
extern void HumanBody_SetBagLeft_mAB9C0E9CB3B621BD7A5DC1242F90AC919005D144 (void);
// 0x0000014B System.Void HumanBody::SetBagRight(UnityEngine.Sprite)
extern void HumanBody_SetBagRight_m90074BC67484801234A29A8C222E46FEE25D62F8 (void);
// 0x0000014C System.Void HumanBody::SetTop(UnityEngine.Sprite)
extern void HumanBody_SetTop_mA3C3410E6E398193513EAADD42431202383F8C56 (void);
// 0x0000014D System.Void HumanBody::.ctor()
extern void HumanBody__ctor_m1402E1C5FB380498069F3F858AB0586B50B04E85 (void);
// 0x0000014E System.Void Item::SetImageItem(UnityEngine.Sprite)
extern void Item_SetImageItem_m467FC8A62555DE47E7953BF57E133BAA7214ED7B (void);
// 0x0000014F System.Void Item::Select()
extern void Item_Select_mD946898684EBC9ADB29C4BB6609153D5CA2262BB (void);
// 0x00000150 System.Void Item::Deselect()
extern void Item_Deselect_m5C408C8C2CE0F79DDE07AD2FB106363B8253B144 (void);
// 0x00000151 System.Void Item::.ctor()
extern void Item__ctor_mDD5CE3191836C8F404C284F0DC5C87394F78E84B (void);
// 0x00000152 T PersistentSingleton`1::get_Instance()
// 0x00000153 System.Void PersistentSingleton`1::Awake()
// 0x00000154 System.Void PersistentSingleton`1::.ctor()
// 0x00000155 T Singleton`1::get_Instance()
// 0x00000156 System.Void Singleton`1::Awake()
// 0x00000157 System.Void Singleton`1::.ctor()
// 0x00000158 System.Void TestOnEditor::Update()
extern void TestOnEditor_Update_m3FAD685D9F029D8576A0238052DD82F602EFF114 (void);
// 0x00000159 System.Void TestOnEditor::UpdateClothes(HumanBoneController)
extern void TestOnEditor_UpdateClothes_m1060161F671757D4E380CE579E7D0C58B4ADA3BB (void);
// 0x0000015A System.Void TestOnEditor::UpdateClothes()
extern void TestOnEditor_UpdateClothes_mDF3A0AD2EE7FD47DF67FD1861F63FDB620BA8566 (void);
// 0x0000015B System.Void TestOnEditor::.ctor()
extern void TestOnEditor__ctor_m321F9911FCF13902EF3DDFF714E7C753056978C0 (void);
// 0x0000015C System.Void UIController::OnChangeValueDropdown()
extern void UIController_OnChangeValueDropdown_mA0A52AC094C8FEF98F95E55B6AD063A9F6F3A730 (void);
// 0x0000015D System.Void UIController::UpdateScrollView(System.Int32)
extern void UIController_UpdateScrollView_mFB56F7273F12A51208109A6C47AF17873452BBF7 (void);
// 0x0000015E System.Collections.Generic.List`1<UnityEngine.Sprite> UIController::GetSpritesInItems(System.Int32)
extern void UIController_GetSpritesInItems_m943A831C64673B3DCB2F0AF504D9DB57AFCF2240 (void);
// 0x0000015F Item UIController::GetEmptyItem(System.Int32)
extern void UIController_GetEmptyItem_mFF60AA395FD14D44B8F984D62F467156562AE36F (void);
// 0x00000160 System.Void UIController::UpdateView(System.Collections.Generic.List`1<UnityEngine.Sprite>)
extern void UIController_UpdateView_m21743E7BB732A34FED7AD072CF266A6F96404FE5 (void);
// 0x00000161 System.Void UIController::OnClickButton(Item)
extern void UIController_OnClickButton_mB0ABFA935114468CBD82429DFCA0F6F6AC170BB8 (void);
// 0x00000162 System.Void UIController::UpdateClothesChosenIndex(System.Int32)
extern void UIController_UpdateClothesChosenIndex_m157D6E9FDF5032F4B84F61C54796B9E4CAD90CB0 (void);
// 0x00000163 System.Void UIController::.ctor()
extern void UIController__ctor_m218DB9E5110A9E50B5F9C61416BB83F05FBBD4EA (void);
// 0x00000164 UnityEngine.Material DilmerGames.Core.Utilities.MaterialUtils::CreateMaterial(UnityEngine.Color,System.String,System.String)
extern void MaterialUtils_CreateMaterial_m9715ACDF80BA0A577845CE38395FF0442CFF56DE (void);
// 0x00000165 T DilmerGames.Core.Singletons.Singleton`1::get_Instance()
// 0x00000166 System.Void DilmerGames.Core.Singletons.Singleton`1::.ctor()
// 0x00000167 System.Void HumanBodyTrackerLogging_<>c::.cctor()
extern void U3CU3Ec__cctor_m72457C7962E0C63AB11C4A803AADE6DB5CEAACBE (void);
// 0x00000168 System.Void HumanBodyTrackerLogging_<>c::.ctor()
extern void U3CU3Ec__ctor_m1555108C8C959965122AFD04E991DC77677E4E28 (void);
// 0x00000169 System.Boolean HumanBodyTrackerLogging_<>c::<ApplyPowers>b__21_0(BoneTracker)
extern void U3CU3Ec_U3CApplyPowersU3Eb__21_0_mD4F101FA3D889E5C74369056A32AF038207B1D33 (void);
// 0x0000016A System.Boolean HumanBodyTrackerLogging_<>c::<ApplyPowers>b__21_1(BoneTracker)
extern void U3CU3Ec_U3CApplyPowersU3Eb__21_1_m05AFC5EE9192042CEE3BEE3A8749D3C1649FB7FD (void);
// 0x0000016B System.Boolean HumanBodyTrackerLogging_<>c::<ApplyPowers>b__21_2(BoneTracker)
extern void U3CU3Ec_U3CApplyPowersU3Eb__21_2_mD9ECEE3C5B27DAFA347488A96B3ECC660FC07E35 (void);
// 0x0000016C System.Void TrackedImageInfoRuntimeCaptureManager_<CaptureImage>d__9::.ctor(System.Int32)
extern void U3CCaptureImageU3Ed__9__ctor_m3B935115C16EC2FCCF5789F5DF98493E1DA99BAC (void);
// 0x0000016D System.Void TrackedImageInfoRuntimeCaptureManager_<CaptureImage>d__9::System.IDisposable.Dispose()
extern void U3CCaptureImageU3Ed__9_System_IDisposable_Dispose_m20FFE472B89533E963C93108B09A4829390D8038 (void);
// 0x0000016E System.Boolean TrackedImageInfoRuntimeCaptureManager_<CaptureImage>d__9::MoveNext()
extern void U3CCaptureImageU3Ed__9_MoveNext_m7A89EECB5E8BEF917704459F364B46BE877083BA (void);
// 0x0000016F System.Object TrackedImageInfoRuntimeCaptureManager_<CaptureImage>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaptureImageU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3710C5BC20FE9FCEA270437927AA7AED7E5EDAC0 (void);
// 0x00000170 System.Void TrackedImageInfoRuntimeCaptureManager_<CaptureImage>d__9::System.Collections.IEnumerator.Reset()
extern void U3CCaptureImageU3Ed__9_System_Collections_IEnumerator_Reset_m8213A7279B4F964695A7BDF795E98E09B67F0583 (void);
// 0x00000171 System.Object TrackedImageInfoRuntimeCaptureManager_<CaptureImage>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CCaptureImageU3Ed__9_System_Collections_IEnumerator_get_Current_mE1C84F23E0EEBAAD236B8C4F7DC6C106BFB82DC3 (void);
// 0x00000172 System.Void TrackedImageInfoRuntimeCaptureManager_<AddImageJob>d__12::.ctor(System.Int32)
extern void U3CAddImageJobU3Ed__12__ctor_m6ED4A0172FACB8D9565229F0F7C8E6DE81FCB4EC (void);
// 0x00000173 System.Void TrackedImageInfoRuntimeCaptureManager_<AddImageJob>d__12::System.IDisposable.Dispose()
extern void U3CAddImageJobU3Ed__12_System_IDisposable_Dispose_mD3BA40F83548B699F3CFC8446B92DAF23C77CAF6 (void);
// 0x00000174 System.Boolean TrackedImageInfoRuntimeCaptureManager_<AddImageJob>d__12::MoveNext()
extern void U3CAddImageJobU3Ed__12_MoveNext_m7597AA0C6602CB42C77B6ADB9BAEAF1D0831492C (void);
// 0x00000175 System.Object TrackedImageInfoRuntimeCaptureManager_<AddImageJob>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAddImageJobU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8FE3363BA2D9A4C6AA0950DC2F494BEB7D11497 (void);
// 0x00000176 System.Void TrackedImageInfoRuntimeCaptureManager_<AddImageJob>d__12::System.Collections.IEnumerator.Reset()
extern void U3CAddImageJobU3Ed__12_System_Collections_IEnumerator_Reset_m7A3E75236AE66D8F5BB2BAB59D8465CBC0590BE8 (void);
// 0x00000177 System.Object TrackedImageInfoRuntimeCaptureManager_<AddImageJob>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CAddImageJobU3Ed__12_System_Collections_IEnumerator_get_Current_m34C264C5C2BD5F07E2FF725AE240E2DE89397C86 (void);
// 0x00000178 System.Void TrackedImageInfoRuntimeSaveManager_<AddImageJob>d__14::.ctor(System.Int32)
extern void U3CAddImageJobU3Ed__14__ctor_m281AF1F5AA92819B021FB905D6B9B806F142B674 (void);
// 0x00000179 System.Void TrackedImageInfoRuntimeSaveManager_<AddImageJob>d__14::System.IDisposable.Dispose()
extern void U3CAddImageJobU3Ed__14_System_IDisposable_Dispose_m35BE1501290CF4819B183027BD5727C12F0E23E1 (void);
// 0x0000017A System.Boolean TrackedImageInfoRuntimeSaveManager_<AddImageJob>d__14::MoveNext()
extern void U3CAddImageJobU3Ed__14_MoveNext_m795250936999440AC378D6D2CB95164D28104FAC (void);
// 0x0000017B System.Object TrackedImageInfoRuntimeSaveManager_<AddImageJob>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAddImageJobU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m514DDF37DB6FF071BB9352E5C9E7A96AAF8013DC (void);
// 0x0000017C System.Void TrackedImageInfoRuntimeSaveManager_<AddImageJob>d__14::System.Collections.IEnumerator.Reset()
extern void U3CAddImageJobU3Ed__14_System_Collections_IEnumerator_Reset_m4300206D9671CDD27C80C62957D06383DE576E12 (void);
// 0x0000017D System.Object TrackedImageInfoRuntimeSaveManager_<AddImageJob>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CAddImageJobU3Ed__14_System_Collections_IEnumerator_get_Current_m12A073C29091EB74174FD85EB4B06B5209CF9131 (void);
// 0x0000017E System.Void UIController_<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m5C08EF00F11D4CF9286AEB84FA31C8BE2E7E520A (void);
// 0x0000017F System.Void UIController_<>c__DisplayClass12_0::<UpdateView>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CUpdateViewU3Eb__0_m4AA71DE8651A15EE8A5193C55B55781FCB5462F5 (void);
// 0x00000180 System.Void UIController_<>c__DisplayClass12_1::.ctor()
extern void U3CU3Ec__DisplayClass12_1__ctor_mDBDD78FE22622AF8D66203B705698643F6859C8B (void);
// 0x00000181 System.Void UIController_<>c__DisplayClass12_1::<UpdateView>b__1()
extern void U3CU3Ec__DisplayClass12_1_U3CUpdateViewU3Eb__1_m06805A4E4E8EE7F4A20C7A3123EF9812B2EFC387 (void);
static Il2CppMethodPointer s_methodPointers[385] = 
{
	csDestroyEffect_Update_m653EBF52C9C3F6A73011A5FD724BAF81A16A0C85,
	csDestroyEffect__ctor_m12E4742C6E2950D223C4B1F24049F51C44F8A134,
	csMouseOrbit_Start_mAEA48A24D38C91EDBDF830718C1C16AB2AEC3841,
	csMouseOrbit_LateUpdate_mC94A662A0BB3AA2AEA85555D599BA304A95DFF0F,
	csMouseOrbit_ClampAngle_m1278F467346C0B15E36A2F3AA63FB85C14924325,
	csMouseOrbit__ctor_m78926A64F3CBD11595E1C30BF66D9C469DC9778C,
	csParticleEffectPackLightControl_Update_mC85A40D3234927737C226DF87126A91A7A58C02A,
	csParticleEffectPackLightControl__ctor_m9724C65D8057AA3C00BFAD52BC7144BB70BD761B,
	csParticleMove_Update_m568A81FC8597439ED0E2BC7D3F87C2802DA62E2A,
	csParticleMove__ctor_m125C12E4DD1294FF1969270296B590DD1632A9F6,
	csShowAllEffect_Start_m06A3C35FDFF54F7E8519B976E06C505E5149BF9F,
	csShowAllEffect_Update_mEF27DC015899FF9BAEA45A7E7EFF05C0F1E491D5,
	csShowAllEffect__ctor_mA7021BB8AD67ED68AC384A503C4E4489CC7A9557,
	ApplyRandomMaterial_Start_m91E40AF66BE87232F9FA45151E05244CE8DBA902,
	ApplyRandomMaterial_GenerateRandomColorForMaterial_mAB28DC52B1EF910C4F669BEBE6A74A1C15B7656F,
	ApplyRandomMaterial_GetRandomColor_m799BCF0423323BD9554E4CFDF786272F2ADD57B6,
	ApplyRandomMaterial__ctor_m42C78367767FC9719C3219F244E364E4C973D23C,
	AutoPlacementOfObjectsInPlane_Awake_m169162C493C5EA0D640F28B037612962F602D841,
	AutoPlacementOfObjectsInPlane_PlaneChanged_mB531E4B70DC8314AA2A23944DE17C394ECD1DC47,
	AutoPlacementOfObjectsInPlane_Dismiss_mB0F9A2FE5235F0D00849CD3F0806E3B3BF1E86B0,
	AutoPlacementOfObjectsInPlane__ctor_mF65F9F4E84F26A189037220FBBCEA9F06A1D3AC4,
	BoneTracker_Start_m9816EF062079FCFB903D3449025D8D851561B969,
	BoneTracker_Update_m81BFEC6F2CC4A9928A9EEFD3F17B28BA67533C6A,
	BoneTracker__ctor_m0F3FF30CCA38BC9227468B3909CE904CCD45995E,
	BuildRig_GetColumn_m94CC96EFF4F5DCDB5C146E32B729DD96D612CFEC,
	BuildRig_Build_mCB61BD2B563F71CA0A64BF857E6079F58EE73CDE,
	BuildRig_Start_m4073A620B3AC6CC5BA6E8503E03081EA2D6ABC19,
	BuildRig_ExtractRotation_mA8B181D7FA399BCF35D6E4E658F9A27D7BBEBE74,
	BuildRig_ExtractPosition_m118A7A6BBD1457F297AEE7003A66E8F1C586685D,
	BuildRig_ExtractScale_m771FF0353BB59143F43E235A25FFFA895D149C11,
	BuildRig_FromMatrix_mB8870173E0D30E02A968A40A8AF8B023380AAF7F,
	BuildRig__ctor_mA88AC25FBF830B5A90B0FFEFBAED393FD744DC01,
	BuildRig__cctor_mD3E6B57DFFF1E26C9F3F7367F0F7C8973813B178,
	CaptureAreaManager_Update_mFBF356AEFA485044F2904CF6FA732733E8160E3F,
	CaptureAreaManager__ctor_m0D55DE70EED0A7C456336FCD09FDDA0FFCF8717E,
	EffectsManager_Start_m176E81CD4BECCCA0766BBE97C4001C00EB957D04,
	EffectsManager_TogglePlaneDetection_mD82C0637F3ABF8BCD6C540D7A88A9FA55FFDCA14,
	EffectsManager_ToggleLights_mCF8EBF0DE4AA7251077A636AAE888A3F7D0FFD0E,
	EffectsManager_ToggleShadows_m0DDAD84F115F68710E2E2A54174B18476AA6E379,
	EffectsManager__ctor_m20B31BB9680029000B91CCE1F74B4E154856CB9A,
	EyeTracker_Awake_m54A20B34D9CFAB9A2A03ECC58A76A73420542898,
	EyeTracker_OnEnable_mDB475A76F5D71784A3EF31E933AC0B09D3BA4205,
	EyeTracker_OnDisable_m66C55BDEB8F7522F564CAAAF5C35EC91A47A44B7,
	EyeTracker_OnUpdated_mF3BD6A7F4A1F0268CA6B4551FC604AA0F4596B64,
	EyeTracker_SetVisibility_m63966D6BFE77487A6FFB32C6FDC0D8149C28BE7C,
	EyeTracker__ctor_mADB9C25EF0F9FC27B15B549661138F1A8BA75200,
	EyeTrackerSupported_OnEnable_m97588929026E0131073052AA36947D274E351333,
	EyeTrackerSupported__ctor_mD94A640B151070609B89C18036152280E410380A,
	FaceController_Awake_m912D2313AD97922CD6A32C36115CA957F3DF3C24,
	FaceController_SwapFaces_m26884D50B7A58F520017514182925D306E6CDB3E,
	FaceController_ToggleTrackingFaces_m1402FBAB1EB75B40E19863BA9C5B0692D40B61DA,
	FaceController__ctor_mD465C8056CA9CE1C5A12DB31BE2AA6A0AF8079F5,
	FaceMaterial__ctor_mD6C4F71CF959CB18D67F536BAABC30D7B3E75278,
	FeatureSupported_Start_m7239FA98F3A9AA310DAF68974A918AC103C2FDA6,
	FeatureSupported__ctor_mC383A584443CB51E32B9933CB193F3446FFF19E2,
	HumanBodyTracker_get_HumanBodyManagers_mB6D61DFBBEF93E1A3C15776E22784FC52E80561C,
	HumanBodyTracker_set_HumanBodyManagers_mA2C6B4E92A9F474E4E8AD8940A0CB2E1C0935F73,
	HumanBodyTracker_get_SkeletonPrefab_m1878E9CA6AF43C1D2C94A1DF623111C5884B18B4,
	HumanBodyTracker_set_SkeletonPrefab_m675784859DA8088A25917F4CCC2FA232169D0B55,
	HumanBodyTracker_OnEnable_m231972EA78E173D2A8175FDF68473FAE3B1AA6F6,
	HumanBodyTracker_OnDisable_mA30452BA079BE71990B6B7DB97C8C3BDCDEA0A1F,
	HumanBodyTracker_OnHumanBodiesChanged_m90A2571C20D330C6D9AAB144DE45A44392F4D261,
	HumanBodyTracker_UpdateClothes_m0955ED75A52C30FBC09BB251992DDBA3BA4147E3,
	HumanBodyTracker__ctor_m454447211F32D1F1F9F366BFBB067CF676A2520E,
	HumanBodyTrackerLogging_get_HumanBodyManagers_mC42C0DB293C13BCA47FBEAF2EBE2A73623C7E4C1,
	HumanBodyTrackerLogging_set_HumanBodyManagers_mDDAA3756715848EADF00F0D322B4A253C278DD73,
	HumanBodyTrackerLogging_get_SkeletonPrefab_m7276D68CF3CDDCF33E1CAFE28B87A8D229D2E473,
	HumanBodyTrackerLogging_set_SkeletonPrefab_m6308DA30DE2CF3A2B6333921BAB24CF6FA511B3E,
	HumanBodyTrackerLogging_Awake_m63E4BF740A2ACF9246C937081655F438BB0BCA0B,
	HumanBodyTrackerLogging_DissmissWelcomePanel_m501BFB23493BC79AC798E9D922C4AA76E7DCE129,
	HumanBodyTrackerLogging_ToggleDebugPanel_m699252C1CC7D8486D258BCBBB19E83557C09342D,
	HumanBodyTrackerLogging_OnEnable_m8DE86567680D20DFC275035DF023AED68A6BFF3B,
	HumanBodyTrackerLogging_OnDisable_m335AF011D77A29F26FF09F997F09B5C7CD4A0D40,
	HumanBodyTrackerLogging_OnHumanBodiesChanged_m479546B4BDBF5DA95C7B89DD07C118B2C101F39E,
	HumanBodyTrackerLogging_ApplyPowers_mA9B45984D0465F97CAC45FA3AF7CF48727679D77,
	HumanBodyTrackerLogging__ctor_m6766028CE5DCAD48602EE144C3260FAE6AD974F3,
	HumanBodyTrackerUI_OnEnable_m6F0BFABC6ADD3259F15A6B21F279A65CBD8B5FCC,
	HumanBodyTrackerUI_Awake_mCDB175AD6175D75031D36D99E3F518128E4E9F14,
	HumanBodyTrackerUI_Dismiss_m276AE77DA2A2A42D6DBAE3C620048C411D16A7E8,
	HumanBodyTrackerUI_ToggleDebugging_mABD7025234BB295BD0B5058BD31B84462A8A3E94,
	HumanBodyTrackerUI_ToggleText_m9666E2FAF10FF936D8FBD27E917B2AC3822F4767,
	HumanBodyTrackerUI_ToggleOptions_mA3850F930FE0A507FBFEE64EBC03A51A8BD9B432,
	HumanBodyTrackerUI__ctor_mB787568466F97D8332E1CB0D6AF893039912A173,
	HumanBodyTrackerWithOptions_get_HumanBodyManagers_mE9E8745BF7AC07B5C55DA26263B3699D5F190735,
	HumanBodyTrackerWithOptions_set_HumanBodyManagers_mECEB734F8EC14090992CA3D25971B0F018AD23F3,
	HumanBodyTrackerWithOptions_get_SkeletonPrefab_mD060B292640C829D092EEF77D9CE807B81372CC6,
	HumanBodyTrackerWithOptions_set_SkeletonPrefab_m86E92CEC9C1F9EAF1124F408AAFB88C226A92499,
	HumanBodyTrackerWithOptions_Awake_mE2E259C7D5457981640823520D857C2AEE71EBB9,
	HumanBodyTrackerWithOptions_OnEnable_m4655C2A1A7F42C5997C1D456529FD465345FA0CB,
	HumanBodyTrackerWithOptions_DissmissWelcomePanel_mD5C78369BABD7B4B3589B33AA22A0C22EC8807D9,
	HumanBodyTrackerWithOptions_OnDisable_m78C5268D2F3CA147944ADCDAF11237B483F71CC2,
	HumanBodyTrackerWithOptions_OnCurveTimeSliderChanged_m4605791BFA1427B9E008CBD1E3DE7E1D98AAB586,
	HumanBodyTrackerWithOptions_OnMinVertexDistanceSliderChanged_m7C93A64A7A0FFCD4DFB2E08DE5B4EE3BF98A36B7,
	HumanBodyTrackerWithOptions_ApplyChangesToCurve_m538E6B7AD8BBEAC27808C577411FB6BC9E9D4FC4,
	HumanBodyTrackerWithOptions_OnHumanBodiesChanged_m4C32543F85D6D412DFB35B349291C53CF74F664D,
	HumanBodyTrackerWithOptions__ctor_m666C6025DE2C2A9D9176003904E7115993299A0C,
	HumanBoneController_get_skeletonRoot_m2E43D2A2F0706D8856B90C9C99789E5691282446,
	HumanBoneController_set_skeletonRoot_m13A3FC33288A924ABBA257B646CF94C09CFA8638,
	HumanBoneController_InitializeSkeletonJoints_m2B1B50242F4CA38FE2B319782D15A097F27BAD4A,
	HumanBoneController_ApplyBodyPose_m315669C85868C33166429DBFE0F567F81784D574,
	HumanBoneController_ProcessJoint_m390BA91B605E4D8B3FBBA5C95C2A56212EB6D7B8,
	HumanBoneController_GetJointIndex_mA7DEE8361A4BA73172924F6E7B88B2410D427147,
	HumanBoneController__ctor_m47A6C770166CBE9881ABEE5B42B0B8C9A2EF3426,
	LightEstimation_Awake_m810C5DDEF38182C155ED925189FCEE5A6156ED0F,
	LightEstimation_OnEnable_m14852BF0988F64CA2BD06CAF46F3F01E0E4ABD8C,
	LightEstimation_OnDisable_mB4EE490FD2CE5C671376ED02803EEEC99DCB6941,
	LightEstimation_FrameUpdated_m7C9F3BF723144C9616A7FA38D0BB6DAFD9734611,
	LightEstimation__ctor_m1D22C524A3F3BACE47DAD18291C9A7265A768E14,
	MeasurementController_Awake_m444D2F091D689C081AE2D2F9BC5FA255CDE5AA20,
	MeasurementController_Dismiss_mCD666A5BD23DDE32FB5E6FA1CD26A959DCD24D82,
	MeasurementController_OnEnable_mE9989E2EE6BC7EA910ED6738FA3CA8558A2E284B,
	MeasurementController_Update_mFDD80DE1273E9C02877CB8E4FDBADF2B2EC46A37,
	MeasurementController__ctor_m7B2E2FA3D26390CFCF1CDC9A32995FF2C8AD4D31,
	MeasurementController__cctor_mE9B8844F7825E1ABFA54A510E12A613AAC87C3B6,
	PeopleOcclusion_Awake_m7FA9AA2785D3A88870DB718AAB021F8B06F9D429,
	PeopleOcclusion_OnEnable_mEA4D53CB6B4024E91075271115B46795D28B2D6C,
	PeopleOcclusion_OnDisable_mFF732BF3E33EE6E56A69F8F0589795CCBBD03513,
	PeopleOcclusion_NextTexture_mBA0EAD6F5E39CF668CDC27D1B60154EC32084176,
	PeopleOcclusion_OnRenderImage_mFBD534D38488FEBA50018D8C02941849919C51FB,
	PeopleOcclusion_OnCameraFrameReceived_m1046CE15B18AAB56DA3C22D485549F474FDFC8F1,
	PeopleOcclusion_PeopleOcclusionSupported_mF663A01F17DEEFCF3ED3261252ACFE98D574CC2C,
	PeopleOcclusion_RefreshCameraFeedTexture_mB0DB6C9AF91FBE4104BCD8EFF258D89BA0718BC8,
	PeopleOcclusion_CalculateUVMultiplierLandScape_m1BD9B2CBA7DB653CA5E85F602436DCEC600C0A20,
	PeopleOcclusion_CalculateUVMultiplierPortrait_mB89FE071BE30CC5C50F504942EA7C75E2853923C,
	PeopleOcclusion__ctor_m34BD43B16F3F109904584AC9B350B17A7E9F63BC,
	PlacementAndLaunchingCanvasController_Awake_mD72433C784D34A7E17B48864195150B1380091A2,
	PlacementAndLaunchingCanvasController_Dismiss_mE7579FB05A5BE220A4B83A408C78D998809D0EB9,
	PlacementAndLaunchingCanvasController_Update_mEFAA0C6BE1B01AF4F3DDC07A53115E2FABF9EB21,
	PlacementAndLaunchingCanvasController__ctor_m3037B08D61479108650A400838A6BDA4F00F3791,
	PlacementBoundingArea_Awake_m706BEE7DFA693F3CCEEBC9C6731D35378CB07D76,
	PlacementBoundingArea_SetupBounds_m0CF087F85054870F897DEAF345EE82D7B1CB282C,
	PlacementBoundingArea_Update_m5853DBECBE1E7F64D5A0D7FC24C01C980E13A531,
	PlacementBoundingArea_DrawBoundingArea_mBC02EC9743D87469D0B307E119F27A20D62135E2,
	PlacementBoundingArea__ctor_mADF3B3A3A174BF960209334C94C93EBD76764577,
	PlacementController_get_PlacedPrefab_m7D6746941CEC265D743AE29DC8013F678222C357,
	PlacementController_set_PlacedPrefab_m637D984CBB79A47B292E9C20592E47CF57A00C9F,
	PlacementController_Awake_m6D3B9373E51E7E9778AF3F89C9F94F0095144D2E,
	PlacementController_TryGetTouchPosition_mAEA65EDBD7E6B64C2C6EAE2E3B3FC862BC5F222A,
	PlacementController_Update_m195CE59EBD6660E533FE832C5BCF0F328A09F7F8,
	PlacementController__ctor_m05CF4708C82DFE821DD99261AED3B0C9E4FC0F4C,
	PlacementController__cctor_m13B32164C0AF1168E34CFBBF8E6E24AE2AC9576B,
	PlacementControllerWithMultiple_Awake_m8AB6E254F9FC049E9F1EE6E3F09DC68AE30CDB5B,
	PlacementControllerWithMultiple_Dismiss_mC7A66D6D7C629CB35CB38C558420F5B985E69C94,
	PlacementControllerWithMultiple_ChangePrefabTo_m5A6815CA78F6834EB2D46EA996883FBC36AA2EB7,
	PlacementControllerWithMultiple_TryGetTouchPosition_m5B4B45EC5291F118EFBEFC8C12B41A17339DDFED,
	PlacementControllerWithMultiple_Update_m7E5F0EB395ADDB25F4BB83105A5A904392B46447,
	PlacementControllerWithMultiple__ctor_mDCB427B596983DFB0991E83529F0CFF1ABA6DAB7,
	PlacementControllerWithMultiple__cctor_m79C8DB39D492570076D62E0555FADEACC26DCE17,
	PlacementControllerWithMultiple_U3CAwakeU3Eb__8_0_m47DDED8B565FDC0872465DCFA8A435008B2A3CC2,
	PlacementControllerWithMultiple_U3CAwakeU3Eb__8_1_m3ACEE2609A4F12865D9B22F2785D7CF1DB3581DE,
	PlacementControllerWithMultiple_U3CAwakeU3Eb__8_2_m781EF4589495177B662FAE7DCF47CC1F5550AF31,
	PlacementEnableAndDisable_get_PlacedPrefab_m77D62F1690E06ED64DFDC9DF92BE62DC0850BD7D,
	PlacementEnableAndDisable_set_PlacedPrefab_m45010BFD529A569D3BAE363935524882B63DA4A2,
	PlacementEnableAndDisable_Awake_m95F7683B691BED72BDD0FD13506ECFABCCF27329,
	PlacementEnableAndDisable_TogglePlaneDetection_mBA84BEA003C774CF7D10B86A649747DDA836A826,
	PlacementEnableAndDisable_ChangePrefabSelection_m5BDF2A46CFB992A42D25EBD98147AC8A0A974297,
	PlacementEnableAndDisable_Dismiss_m4F987C404DE2647C1D68234526B7DDD40AC24037,
	PlacementEnableAndDisable_Update_mAEA59A079B26B40F3BC7A730C509740FCB8F4AB6,
	PlacementEnableAndDisable__ctor_m367DAE2BCCC2CB00F8E8445E900465D49244848E,
	PlacementEnableAndDisable__cctor_m516BB221E7F1F59E63A238DACD388C0F2A6C5C2D,
	PlacementEnableAndDisable_U3CAwakeU3Eb__18_0_mBC48A5D3E233CADCAEFE7744E55DE3C2D5A2FCF3,
	PlacementEnableAndDisable_U3CAwakeU3Eb__18_1_m5814B3B83D3414804285C1D3E0F9774793AABEF8,
	PlacementEnableAndDisable_U3CAwakeU3Eb__18_2_mED953F4CDED930B9AB5B109054C5925349CAD9EE,
	PlacementObject_get_Selected_mA798C2D2BF95A80BBADBB8315E701A78E612ADEB,
	PlacementObject_set_Selected_m45896B6FE7BA5BABBED21C2EC5A142C97BF98697,
	PlacementObject_get_Locked_m32A3EB23EF8D75B807062D86FBDF7F83EB40A48B,
	PlacementObject_set_Locked_mD8B284B7723E7CA333CBDA6C198A7DB793F3BE17,
	PlacementObject_SetOverlayText_m46C49B68F6237B68F7F5B58603B08096E37ADCA5,
	PlacementObject_Awake_m2D81C5E6930C59846362DAF863A39AC90B24D15C,
	PlacementObject_ToggleOverlay_m30DAA7869ECAC96E6751844C6974FD35AED55C11,
	PlacementObject_ToggleCanvas_mA9B9B37C562321A9E9706AF5FB06C4F14ADBD17A,
	PlacementObject__ctor_m39577B21713D7D5F4502865A4C4024D35C7212CC,
	PlacementRotation_Awake_mE77D4B707C0A5376AA455CD3D36BF5C108380257,
	PlacementRotation_Update_mEFF34A6031D4CE73E2794BA665A62E15BAFB9010,
	PlacementRotation__ctor_m810455AACD54490C47BAB24621C981509290F541,
	PlacementWithDraggingDroppingController_Awake_m94769FE3B6350B058977089203E1CC49174CF5B8,
	PlacementWithDraggingDroppingController_Dismiss_m8B27CBA48217084982978776C4530B5AA8A732A7,
	PlacementWithDraggingDroppingController_Lock_mA6F7FACA1D39F4F9AE5EE09AB4BF4DA26BD3A7F5,
	PlacementWithDraggingDroppingController_Update_m40F91837A23F99F6B822103B5960A463A8F86FB2,
	PlacementWithDraggingDroppingController__ctor_m998CCA8F66D84DC2700D274DD5BF3B5C3FB86409,
	PlacementWithDraggingDroppingController__cctor_m661DE9D762D8A6E80205057AC15498A854E083A4,
	PlacementWithManyController_get_PlacedPrefab_m64312779DF8AB7D0AF2477ED6ED9CC467C020C60,
	PlacementWithManyController_set_PlacedPrefab_m11A7C071E8E481A49C67DA879DC37EF425D2EDB4,
	PlacementWithManyController_Awake_m36755D3C13265428DB2B076AA2F8BE2FECC51E3F,
	PlacementWithManyController_Dismiss_m6683B912D4D274010C518C82530E7008941380C7,
	PlacementWithManyController_Update_mC4CED3977271C1C19D1809D579DB55B20283ECE2,
	PlacementWithManyController__ctor_m72B73173047BF7921D520FAD3167568FDA9549C3,
	PlacementWithManyController__cctor_m8392C0A431A8DF479E6EFCE10FD9BD89697A8A7F,
	PlacementWithManyObjectsController_Awake_mAC420E6EB17B9AD6E7D6F0B4BE388A2F97685FF8,
	PlacementWithManyObjectsController_Dismiss_m7E771D455CA010E31A99C4730B90A5055D30CF0D,
	PlacementWithManyObjectsController_Update_m7B53CD7FCA505CA83C5D5DB0921F0EA977D4E21A,
	PlacementWithManyObjectsController__ctor_m855484C27F13842C6DBAB591013CCC41DCB6F86C,
	PlacementWithManyObjectsController__cctor_m728ED2A1F6780E44B661CD8D221EF2FD3259195D,
	PlacementWithManySelectionController_Awake_m7FB46F58056E1D329CC7BCB2CC73BEE992F7F189,
	PlacementWithManySelectionController_Start_m6A46DD8E87A26FC70D3B85E47975D4FE20D887B9,
	PlacementWithManySelectionController_Dismiss_m848938B2B202E58604F8647061450E3EA8C53771,
	PlacementWithManySelectionController_Update_m2B7944FC1D933F85AFD18957932C8C9C30B5FB8F,
	PlacementWithManySelectionController_ChangeSelectedObject_m880A8FCA5020393D6E01536D1DF5C19FA01EFD32,
	PlacementWithManySelectionController__ctor_m5F4AF98465F91D60DFB003B45C88A673250DDAD4,
	PlacementWithManySelectionWithScaleController_get_PlacedPrefab_m15D353C84B54ED5E347B9DBBE08F0105C9AD24BC,
	PlacementWithManySelectionWithScaleController_set_PlacedPrefab_mB2C8AD3A2AF5C83B95B0ECC1082F8A6F5523BE92,
	PlacementWithManySelectionWithScaleController_Awake_mD2008B3350230B99FB0E51CC4D1953D1651C7E4F,
	PlacementWithManySelectionWithScaleController_ToggleOptions_m664C1D3813269AEC288E60CA6755656EFCC0F596,
	PlacementWithManySelectionWithScaleController_Dismiss_m880426D5AABF5F88E63DC69AA495744CCA794BCA,
	PlacementWithManySelectionWithScaleController_ScaleChanged_m09B54B0D8CD6802BC9D8EC503CF0FB0AB7CD79BE,
	PlacementWithManySelectionWithScaleController_Update_mF3589DEE8158AF9C5585A1A0040DB5A594920DCE,
	PlacementWithManySelectionWithScaleController__ctor_m7FAD25C506E5770F71456CEBF58FA2581777AE7E,
	PlacementWithManySelectionWithScaleController__cctor_m3B90CBCDB02B89E46570C9AAF73951982A9B6262,
	PlacementWithManySinglePrefabSelectionController_get_PlacedPrefab_mB3E6EE1F3C34F436A223BD8836DC2C5FFCF10D16,
	PlacementWithManySinglePrefabSelectionController_set_PlacedPrefab_m70B9EBFB2AAA2AF88E3F40CAD7364D261A6EAEF6,
	PlacementWithManySinglePrefabSelectionController_Awake_m7F4ADEC2626E71E58B892538B25E7601D84A51D1,
	PlacementWithManySinglePrefabSelectionController_Dismiss_m7CB7156BC1C41F26B9E73B9AD5D3A02AE83BD1C8,
	PlacementWithManySinglePrefabSelectionController_Update_m4F03734D37CD1306EC8A7012B2E90B1EA390566D,
	PlacementWithManySinglePrefabSelectionController__ctor_m986C0913F19316F9A03058449D865401C592D603,
	PlacementWithManySinglePrefabSelectionController__cctor_m694A1B31C3A14E629704090B495A45CD0EAE9AF2,
	PlacementWithMultipleDraggingDroppingController_get_PlacedPrefab_m2317A7C4B67054A82F567A44033C608620774B63,
	PlacementWithMultipleDraggingDroppingController_set_PlacedPrefab_m35573B9B9C397F1495063452AB3513FFB8186289,
	PlacementWithMultipleDraggingDroppingController_Awake_m32EEAF62A803B2B80C3F083359C7F4E3E3FCE948,
	PlacementWithMultipleDraggingDroppingController_ChangePrefabSelection_mD8663D76FC662C424A99C93BE2F37D52C1A1FDE5,
	PlacementWithMultipleDraggingDroppingController_Dismiss_m74A55FEB11266B43F819164E9BE0027DD9C1A22E,
	PlacementWithMultipleDraggingDroppingController_Update_m0FE9FD6D19EA032A8B476ACB95CA43C1C89BB0C9,
	PlacementWithMultipleDraggingDroppingController__ctor_m64E3AA08A57BB231C8013CC52307D2C0B2195759,
	PlacementWithMultipleDraggingDroppingController__cctor_mCA8A673AE84CDC8B440E4B3431CC5D5D6B545BD7,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_0_m7976306C9AD1C7A08A9FF89809C71BE67DB8A8A4,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_1_m53BC05EB8FF55B5C927458A90CDE8E090B877094,
	PlacementWithMultipleDraggingDroppingController_U3CAwakeU3Eb__16_2_m37D674BD4783C0C61CE485C6B4121DADCEADB615,
	ReferencePointManager_Awake_mC60B2B8B1E2CF0FF5C44D32E5DD8EDE5C4CE21B1,
	ReferencePointManager_Update_mDE36DD75684902F71C2A888EFBD7105DCF76AE85,
	ReferencePointManager_TogglePlaneDetection_m1830388CE1571D8DC4F353812933D5174D6DBD93,
	ReferencePointManager_ClearReferencePoints_m215DB023849FB84311297EB2056AF9A4456BC4EE,
	ReferencePointManager__ctor_m755FCB9492698121064B9774242BA587549003DA,
	ReferencePointManager__cctor_m3F9F7C5DC360D4EAE489ACDFCBFEF0A9CD84C483,
	ReferencePointManagerWithCameraDistance_Awake_m9BB036D0FE2A0547B21EFEA42BC2D36025A8344F,
	ReferencePointManagerWithCameraDistance_Update_m6B8E7E7E76B1F9EDC14129B84C52968E093C5992,
	ReferencePointManagerWithCameraDistance_ClearReferencePoints_m182123D304A87D379A8F19C08635CA0BD8AFDA50,
	ReferencePointManagerWithCameraDistance__ctor_mD9AF232E9DFFD733A9BAC6BCA7184D2523683A16,
	ReferencePointManagerWithFeaturePoints_Awake_mB375336F2234CD91C996EFAB1A13727C7A828937,
	ReferencePointManagerWithFeaturePoints_Update_mCABF123DC8B9FC12D9451004F47AE076A008EE06,
	ReferencePointManagerWithFeaturePoints_ToggleDetection_mD1448A8A43FF6EB9C46244953AECECDAE618DAA0,
	ReferencePointManagerWithFeaturePoints_ClearReferencePoints_mB04EC353222171D2388E91139C3788635A442EE2,
	ReferencePointManagerWithFeaturePoints__ctor_m39879B98823F456CB185B24558B6BEA078958D1C,
	ReferencePointManagerWithFeaturePoints__cctor_mCAC5552969771A57CA1DBDFC1E9D4705123C41D1,
	ScreenSpaceJointVisualizer_get_arCamera_m128BEF2E57E20E4427EE9826A916A1B087492BBD,
	ScreenSpaceJointVisualizer_set_arCamera_m6BC0FB3BE4A119B6AFE9704BF4BE2C367149334D,
	ScreenSpaceJointVisualizer_get_humanBodyManager_m734494DABEDD13584C84EA0E68D99081E224DD54,
	ScreenSpaceJointVisualizer_set_humanBodyManager_mA323AE1C7FBC486559B35DD43B9048A50640E2F7,
	ScreenSpaceJointVisualizer_get_lineRendererPrefab_m10E2D0BD900B08385DA937E593F76052D0D1FFF8,
	ScreenSpaceJointVisualizer_set_lineRendererPrefab_m8E4678D5F42488BD9B47C55934469B345DA67775,
	ScreenSpaceJointVisualizer_Awake_m28374A2BEC6437A3BF5DACF9312B519252179EB7,
	ScreenSpaceJointVisualizer_UpdateRenderer_m9F1206B04515C4744A44EE9B44E7203F21A1AE96,
	ScreenSpaceJointVisualizer_Update_mEC032154B52D456A88863E838CC7784D7CD5F59D,
	ScreenSpaceJointVisualizer_HideJointLines_mED607BF3196A13BF2B1FE5BCDAE6F44E2013E592,
	ScreenSpaceJointVisualizer__ctor_m20024FC7F92DC5B513D5D2A9CD95168B13227E43,
	ScreenSpaceJointVisualizer__cctor_mC44F836911817EE0D1F4CBCD7713879DFD29F783,
	SelectionFromCameraOrigin_Awake_m12D4829DC922B9AC29423451F66B6A46D389576E,
	SelectionFromCameraOrigin_Start_mF103AAAB91D6B6BCCCD1DE0C02FDF83E03F86918,
	SelectionFromCameraOrigin_Dismiss_m2CB361B419646011DFC5F785AF8FFECBDCAF0143,
	SelectionFromCameraOrigin_Update_m8E8133D652FE36580E52197663CDC6BB6FF9CB16,
	SelectionFromCameraOrigin_ChangeSelectedObject_m73400F6198BC67FD04CFC60EAF34FDE3A43115B9,
	SelectionFromCameraOrigin__ctor_mCE14DB4DC5D8B285A0D47C6DBB97D7E2242626B9,
	SimpleUI_Awake_mA08B2FA6E73CAF74D315F82D99E989F17C0D759A,
	SimpleUI_Dismiss_mC78EF091D89E53ADF02AFA6E9D21631F073A15EF,
	SimpleUI__ctor_m510966DDE395DEA4E64FB8797E8AFC0ADF068650,
	SpawnSuperPowers_get_Started_mDE334338F6962B73EB45427507B2BB9CE9DF1156,
	SpawnSuperPowers_set_Started_m6620955416CA9DE4ADE3E996DFD5D07D35C42A7D,
	SpawnSuperPowers_FixedUpdate_m930A8924E396F1CBEE75A2A8A32F2CB16235B139,
	SpawnSuperPowers_ApplyForce_m8F3A9981FEC5BF70B01C52F4B07C2AE6DD0ED566,
	SpawnSuperPowers__ctor_m58E20D5C3A06B7441A18FD09611D8B43582F1C2A,
	TrackedImageInfoExtendedManager_Awake_m598F7103464F96AA1E3D0282279721D92F05F702,
	TrackedImageInfoExtendedManager_OnEnable_m37398ECCAC0BC7F91EFF3C8A5D23DD6D065AF41A,
	TrackedImageInfoExtendedManager_OnDisable_mAE5AB0613E353E38FF17A8CA2E6F83F3D552ABC3,
	TrackedImageInfoExtendedManager_Dismiss_m7FFFA4AF326EBA03F00C77C2ECA769D90383C0D1,
	TrackedImageInfoExtendedManager_OnTrackedImagesChanged_m9B912C4F9073B8409CCC31051A9913BB51300CC3,
	TrackedImageInfoExtendedManager__ctor_m1F61639687AD368FF12822A90AF778AD4C633D15,
	TrackedImageInfoManager_Awake_mF2314D24BAD4E61E111D4483DD3B7BE0F47D0F34,
	TrackedImageInfoManager_OnEnable_mF5628468167127206B176871AA5B646BEE851B3D,
	TrackedImageInfoManager_OnDisable_mBAA28BD3CBAA1EEEADFB9A224E68FC07D072D609,
	TrackedImageInfoManager_OnTrackedImagesChanged_m9A6FB567BDFF58A96A14427F0F455AD02D58F6BB,
	TrackedImageInfoManager__ctor_m3B347DDF375D07B60D343A6A2E533E50CA114AD8,
	TrackedImageInfoMultipleManager_Awake_m6006A76CA1F3CD93DC9B0F468D53ADFDDB3931D2,
	TrackedImageInfoMultipleManager_OnEnable_m1D976A8CA5A72A97E52CC245D1A74A09C5A74FD3,
	TrackedImageInfoMultipleManager_OnDisable_mD3934CADCA532033C34C7602EB7A277322A387DC,
	TrackedImageInfoMultipleManager_Dismiss_mD6BCE727BDC6A4FBBB815763A812AAE8E54D82FB,
	TrackedImageInfoMultipleManager_OnTrackedImagesChanged_mE6FEB63B99B2FB6B9F120D7108E0F40FD9B6E780,
	TrackedImageInfoMultipleManager_UpdateARImage_m83BB6831E4C936032FEFF3A48DF751836D111FC6,
	TrackedImageInfoMultipleManager_AssignGameObject_mAFE5792C68403222B93CA862A6C3175BDC0754DB,
	TrackedImageInfoMultipleManager__ctor_m9E2072969FF1A5EB488216623E429AFA3804FE9C,
	TrackedImageInfoRuntimeCaptureManager_Start_m2750A2922CD5F1A6D8367DAF2097A1B2AF0C01C5,
	TrackedImageInfoRuntimeCaptureManager_CaptureImage_m7BE57B8159F2008F80C775FC32FDC7D781E7D622,
	TrackedImageInfoRuntimeCaptureManager_ShowTrackerInfo_mB7CEAD73A140C064D3FF86A64F4085A6AB751080,
	TrackedImageInfoRuntimeCaptureManager_OnDisable_m9C96260B90207C430E28A2F64D173B31BA0247AA,
	TrackedImageInfoRuntimeCaptureManager_AddImageJob_m54A9F8A078B420007F3FD2F5AD30BAB47B6FA08A,
	TrackedImageInfoRuntimeCaptureManager_OnTrackedImagesChanged_m296C47CA2342BAB81E267B166AF814843E18F6FB,
	TrackedImageInfoRuntimeCaptureManager__ctor_m92A045EBD4A49DA493040BF763F8D72ACDE39CDA,
	TrackedImageInfoRuntimeCaptureManager_U3CStartU3Eb__8_0_m426415C76783782DB61B42594FA640D57FF2BFA8,
	TrackedImageInfoRuntimeManager_Start_m573EA728E21437B35DAFA93EE87CE01786E87101,
	TrackedImageInfoRuntimeManager_ShowTrackerInfo_m2483A8F87CEC50872E543B478FB5456D5AA8BCD2,
	TrackedImageInfoRuntimeManager_OnDisable_m5BD533C33D2A10B6FC1EA2EE0D9EB17FB626C88F,
	TrackedImageInfoRuntimeManager_OnTrackedImagesChanged_m76866B5646E66565E95D231159016AEF99E2DDBC,
	TrackedImageInfoRuntimeManager__ctor_mCC91B31B5330089806F5657E5592A80E63C1082D,
	TrackedImageInfoRuntimeMultipleManager_Start_m3CBEE333BAF1E38C391D531D202AC3B908503B05,
	TrackedImageInfoRuntimeMultipleManager_SetReferenceImageLibrary_mFAD6898595E64161184F2CA143F946C7C0CEB121,
	TrackedImageInfoRuntimeMultipleManager_ShowTrackerInfo_m4E1686BFDB7A2A2C03222E1C91AB3FFA5CEC7035,
	TrackedImageInfoRuntimeMultipleManager_OnDisable_m4608352878B72933A9B9C3EA76606BAE974C71FF,
	TrackedImageInfoRuntimeMultipleManager_OnTrackedImagesChanged_m73A812558BD3DE4C8A7B034A53D694EE4F1F5E29,
	TrackedImageInfoRuntimeMultipleManager__ctor_mB80780F2B5A0823A1DAE542CF81F443A147A02F0,
	TrackedImageInfoRuntimeMultipleManager_U3CStartU3Eb__9_0_m73E19B9EE4A09A802DD12B52E3A4FB8EF9B7072A,
	TrackedImageInfoRuntimeMultipleManager_U3CStartU3Eb__9_1_mE648F38B46602FEEB4835AC1DD3814F0DD70E464,
	TrackedImageInfoRuntimeSaveManager_Start_m5BCF99CCA0FC231313AD1B89E657A275E054C8B9,
	TrackedImageInfoRuntimeSaveManager_ShowTrackerInfo_m8E3B04D117A3CB10BC68D758DBF121EA2F916EA3,
	TrackedImageInfoRuntimeSaveManager_OnDisable_m3ACE6963B3DD7DFD3AC1BDCE328C38CBFE187F3F,
	TrackedImageInfoRuntimeSaveManager_AddImageJob_mC13E2B55FF35D4E2471DF83211DD8D88AF5927C0,
	TrackedImageInfoRuntimeSaveManager_OnTrackedImagesChanged_mC5F4554F98F4D49BC09F27C4B8BDCF1641F38F5E,
	TrackedImageInfoRuntimeSaveManager__ctor_m8BC492C71CD67CFFF94C8AED1A362E59FCD8FED8,
	TrackedImageInfoRuntimeSaveManager_U3CStartU3Eb__11_0_mD0A2F285502F85EEDEEAAEE57266D02E53D24241,
	TrackedImageInfoRuntimeSaveManager_U3CStartU3Eb__11_1_mAB6681BC849C22F851D681A7B691AFDDBB8186C7,
	AutoTransformDirection_Update_mEB9BD2A555569E05A4AA37760724FB94F82C9758,
	AutoTransformDirection__ctor_m5D6D4E92193085DC73F4D0F5709911302F079818,
	ClothesController_Start_mF7E566841469D59949208962E28785839FCE49F8,
	ClothesController_GetSpritesClothes_m9185F22E0DC8929CCE1304C49FB958B84EF0E74E,
	ClothesController_AddToList_m4B70988B9E8A54E3A77E6D98339BF74CE2BC25B0,
	ClothesController__ctor_mCBE54B7D07721A8A7862C8171F9816B77549D841,
	HumanBody_SpawnCloth_mC0262DE6AAA118C5BD2D8AAD33A8CE6A03FD560A,
	HumanBody_UpdateClothes_mD03E6107E6C41B2040296F9A22CF9ABEFB7253C4,
	HumanBody_UpdateClothes_mA16BAB7D07B9AB691265FEDA783812874835B290,
	HumanBody_SetDress_m53D2192885BECC156EF6757F7CC358BAF2111D5D,
	HumanBody_SetShirt_mD4BEFFDD712584E1FB5A6F09209834C84AF9E32B,
	HumanBody_SetHat_mCF228169A979EEF9E214113577DF52B228C7C5A6,
	HumanBody_SetTie_m446D7AAFC145AB2974B42417477C52CB1850C4DE,
	HumanBody_SetBagLeft_mAB9C0E9CB3B621BD7A5DC1242F90AC919005D144,
	HumanBody_SetBagRight_m90074BC67484801234A29A8C222E46FEE25D62F8,
	HumanBody_SetTop_mA3C3410E6E398193513EAADD42431202383F8C56,
	HumanBody__ctor_m1402E1C5FB380498069F3F858AB0586B50B04E85,
	Item_SetImageItem_m467FC8A62555DE47E7953BF57E133BAA7214ED7B,
	Item_Select_mD946898684EBC9ADB29C4BB6609153D5CA2262BB,
	Item_Deselect_m5C408C8C2CE0F79DDE07AD2FB106363B8253B144,
	Item__ctor_mDD5CE3191836C8F404C284F0DC5C87394F78E84B,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	TestOnEditor_Update_m3FAD685D9F029D8576A0238052DD82F602EFF114,
	TestOnEditor_UpdateClothes_m1060161F671757D4E380CE579E7D0C58B4ADA3BB,
	TestOnEditor_UpdateClothes_mDF3A0AD2EE7FD47DF67FD1861F63FDB620BA8566,
	TestOnEditor__ctor_m321F9911FCF13902EF3DDFF714E7C753056978C0,
	UIController_OnChangeValueDropdown_mA0A52AC094C8FEF98F95E55B6AD063A9F6F3A730,
	UIController_UpdateScrollView_mFB56F7273F12A51208109A6C47AF17873452BBF7,
	UIController_GetSpritesInItems_m943A831C64673B3DCB2F0AF504D9DB57AFCF2240,
	UIController_GetEmptyItem_mFF60AA395FD14D44B8F984D62F467156562AE36F,
	UIController_UpdateView_m21743E7BB732A34FED7AD072CF266A6F96404FE5,
	UIController_OnClickButton_mB0ABFA935114468CBD82429DFCA0F6F6AC170BB8,
	UIController_UpdateClothesChosenIndex_m157D6E9FDF5032F4B84F61C54796B9E4CAD90CB0,
	UIController__ctor_m218DB9E5110A9E50B5F9C61416BB83F05FBBD4EA,
	MaterialUtils_CreateMaterial_m9715ACDF80BA0A577845CE38395FF0442CFF56DE,
	NULL,
	NULL,
	U3CU3Ec__cctor_m72457C7962E0C63AB11C4A803AADE6DB5CEAACBE,
	U3CU3Ec__ctor_m1555108C8C959965122AFD04E991DC77677E4E28,
	U3CU3Ec_U3CApplyPowersU3Eb__21_0_mD4F101FA3D889E5C74369056A32AF038207B1D33,
	U3CU3Ec_U3CApplyPowersU3Eb__21_1_m05AFC5EE9192042CEE3BEE3A8749D3C1649FB7FD,
	U3CU3Ec_U3CApplyPowersU3Eb__21_2_mD9ECEE3C5B27DAFA347488A96B3ECC660FC07E35,
	U3CCaptureImageU3Ed__9__ctor_m3B935115C16EC2FCCF5789F5DF98493E1DA99BAC,
	U3CCaptureImageU3Ed__9_System_IDisposable_Dispose_m20FFE472B89533E963C93108B09A4829390D8038,
	U3CCaptureImageU3Ed__9_MoveNext_m7A89EECB5E8BEF917704459F364B46BE877083BA,
	U3CCaptureImageU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3710C5BC20FE9FCEA270437927AA7AED7E5EDAC0,
	U3CCaptureImageU3Ed__9_System_Collections_IEnumerator_Reset_m8213A7279B4F964695A7BDF795E98E09B67F0583,
	U3CCaptureImageU3Ed__9_System_Collections_IEnumerator_get_Current_mE1C84F23E0EEBAAD236B8C4F7DC6C106BFB82DC3,
	U3CAddImageJobU3Ed__12__ctor_m6ED4A0172FACB8D9565229F0F7C8E6DE81FCB4EC,
	U3CAddImageJobU3Ed__12_System_IDisposable_Dispose_mD3BA40F83548B699F3CFC8446B92DAF23C77CAF6,
	U3CAddImageJobU3Ed__12_MoveNext_m7597AA0C6602CB42C77B6ADB9BAEAF1D0831492C,
	U3CAddImageJobU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE8FE3363BA2D9A4C6AA0950DC2F494BEB7D11497,
	U3CAddImageJobU3Ed__12_System_Collections_IEnumerator_Reset_m7A3E75236AE66D8F5BB2BAB59D8465CBC0590BE8,
	U3CAddImageJobU3Ed__12_System_Collections_IEnumerator_get_Current_m34C264C5C2BD5F07E2FF725AE240E2DE89397C86,
	U3CAddImageJobU3Ed__14__ctor_m281AF1F5AA92819B021FB905D6B9B806F142B674,
	U3CAddImageJobU3Ed__14_System_IDisposable_Dispose_m35BE1501290CF4819B183027BD5727C12F0E23E1,
	U3CAddImageJobU3Ed__14_MoveNext_m795250936999440AC378D6D2CB95164D28104FAC,
	U3CAddImageJobU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m514DDF37DB6FF071BB9352E5C9E7A96AAF8013DC,
	U3CAddImageJobU3Ed__14_System_Collections_IEnumerator_Reset_m4300206D9671CDD27C80C62957D06383DE576E12,
	U3CAddImageJobU3Ed__14_System_Collections_IEnumerator_get_Current_m12A073C29091EB74174FD85EB4B06B5209CF9131,
	U3CU3Ec__DisplayClass12_0__ctor_m5C08EF00F11D4CF9286AEB84FA31C8BE2E7E520A,
	U3CU3Ec__DisplayClass12_0_U3CUpdateViewU3Eb__0_m4AA71DE8651A15EE8A5193C55B55781FCB5462F5,
	U3CU3Ec__DisplayClass12_1__ctor_mDBDD78FE22622AF8D66203B705698643F6859C8B,
	U3CU3Ec__DisplayClass12_1_U3CUpdateViewU3Eb__1_m06805A4E4E8EE7F4A20C7A3123EF9812B2EFC387,
};
static const int32_t s_InvokerIndices[385] = 
{
	23,
	23,
	23,
	23,
	1942,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1097,
	23,
	23,
	1943,
	23,
	23,
	23,
	23,
	23,
	1944,
	23,
	23,
	1945,
	1946,
	1946,
	1947,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1761,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	1948,
	26,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	1948,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	275,
	275,
	26,
	1948,
	23,
	14,
	26,
	23,
	1818,
	26,
	104,
	23,
	23,
	23,
	23,
	1739,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	27,
	1739,
	102,
	23,
	181,
	181,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	14,
	26,
	23,
	762,
	23,
	23,
	3,
	23,
	23,
	26,
	762,
	23,
	23,
	3,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	26,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	102,
	31,
	102,
	31,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	14,
	26,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	26,
	23,
	23,
	23,
	275,
	23,
	23,
	3,
	14,
	26,
	23,
	23,
	23,
	23,
	3,
	14,
	26,
	23,
	26,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	1949,
	23,
	23,
	23,
	3,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	102,
	31,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	1950,
	23,
	23,
	23,
	23,
	1950,
	23,
	23,
	23,
	23,
	23,
	1950,
	26,
	1818,
	23,
	23,
	14,
	23,
	23,
	28,
	1950,
	23,
	23,
	23,
	23,
	23,
	1950,
	23,
	23,
	32,
	23,
	23,
	1950,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	1950,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	35,
	23,
	26,
	26,
	870,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	26,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	23,
	26,
	23,
	23,
	23,
	32,
	34,
	34,
	26,
	26,
	32,
	23,
	1951,
	-1,
	-1,
	3,
	23,
	9,
	9,
	9,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	32,
	23,
	102,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[3] = 
{
	{ 0x0200003A, { 0, 4 } },
	{ 0x0200003B, { 4, 4 } },
	{ 0x02000040, { 8, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[13] = 
{
	{ (Il2CppRGCTXDataType)2, 18123 },
	{ (Il2CppRGCTXDataType)2, 17855 },
	{ (Il2CppRGCTXDataType)3, 13297 },
	{ (Il2CppRGCTXDataType)3, 13298 },
	{ (Il2CppRGCTXDataType)2, 18124 },
	{ (Il2CppRGCTXDataType)2, 17859 },
	{ (Il2CppRGCTXDataType)3, 13299 },
	{ (Il2CppRGCTXDataType)3, 13300 },
	{ (Il2CppRGCTXDataType)2, 18125 },
	{ (Il2CppRGCTXDataType)2, 17879 },
	{ (Il2CppRGCTXDataType)1, 17879 },
	{ (Il2CppRGCTXDataType)2, 18126 },
	{ (Il2CppRGCTXDataType)3, 13301 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	385,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	3,
	s_rgctxIndices,
	13,
	s_rgctxValues,
	NULL,
};
