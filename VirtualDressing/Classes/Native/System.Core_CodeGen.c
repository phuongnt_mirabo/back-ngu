﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x00000002 System.Exception System.Linq.Error::MoreThanOneElement()
extern void Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863 (void);
// 0x00000003 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000004 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000005 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000006 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000007 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000008 System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x00000009 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000A System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000B TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000C TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000D TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000000E System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000000F System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000010 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000011 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000012 System.Void System.Linq.Enumerable_Iterator`1::.ctor()
// 0x00000013 TSource System.Linq.Enumerable_Iterator`1::get_Current()
// 0x00000014 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_Iterator`1::Clone()
// 0x00000015 System.Void System.Linq.Enumerable_Iterator`1::Dispose()
// 0x00000016 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable_Iterator`1::GetEnumerator()
// 0x00000017 System.Boolean System.Linq.Enumerable_Iterator`1::MoveNext()
// 0x00000018 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000019 System.Object System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x0000001A System.Collections.IEnumerator System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000001B System.Void System.Linq.Enumerable_Iterator`1::System.Collections.IEnumerator.Reset()
// 0x0000001C System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000001D System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Clone()
// 0x0000001E System.Void System.Linq.Enumerable_WhereEnumerableIterator`1::Dispose()
// 0x0000001F System.Boolean System.Linq.Enumerable_WhereEnumerableIterator`1::MoveNext()
// 0x00000020 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000021 System.Void System.Linq.Enumerable_WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x00000022 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Clone()
// 0x00000023 System.Boolean System.Linq.Enumerable_WhereArrayIterator`1::MoveNext()
// 0x00000024 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000025 System.Void System.Linq.Enumerable_WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000026 System.Linq.Enumerable_Iterator`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Clone()
// 0x00000027 System.Boolean System.Linq.Enumerable_WhereListIterator`1::MoveNext()
// 0x00000028 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable_WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000029 System.Void System.Linq.Enumerable_<>c__DisplayClass6_0`1::.ctor()
// 0x0000002A System.Boolean System.Linq.Enumerable_<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x0000002B System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x0000002C System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x0000002D System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x0000002E System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000002F System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000030 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x00000031 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::.ctor(System.Int32)
// 0x00000032 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x00000033 System.Boolean System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::MoveNext()
// 0x00000034 TElement System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x00000035 System.Void System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x00000036 System.Object System.Linq.OrderedEnumerable`1_<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x00000037 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x00000038 System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x00000039 System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x0000003A System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x0000003B System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x0000003C System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x0000003D System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x0000003E System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x0000003F System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x00000040 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x00000041 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x00000042 TElement[] System.Linq.Buffer`1::ToArray()
// 0x00000043 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x00000044 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x00000045 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000046 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x00000047 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x00000048 System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x00000049 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x0000004A System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x0000004B System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x0000004C System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x0000004D System.Collections.Generic.HashSet`1_Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x0000004E System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x0000004F System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000050 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x00000051 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x00000052 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x00000053 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x00000054 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000055 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x00000056 System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x00000057 System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x00000058 System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x00000059 System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x0000005A System.Void System.Collections.Generic.HashSet`1_Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x0000005B System.Void System.Collections.Generic.HashSet`1_Enumerator::Dispose()
// 0x0000005C System.Boolean System.Collections.Generic.HashSet`1_Enumerator::MoveNext()
// 0x0000005D T System.Collections.Generic.HashSet`1_Enumerator::get_Current()
// 0x0000005E System.Object System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.get_Current()
// 0x0000005F System.Void System.Collections.Generic.HashSet`1_Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[95] = 
{
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_MoreThanOneElement_mD96D1249F5D42379E9417302B5F33DD99B51C863,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[95] = 
{
	0,
	4,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[27] = 
{
	{ 0x02000004, { 45, 4 } },
	{ 0x02000005, { 49, 9 } },
	{ 0x02000006, { 58, 7 } },
	{ 0x02000007, { 65, 10 } },
	{ 0x02000008, { 75, 1 } },
	{ 0x0200000A, { 76, 3 } },
	{ 0x0200000B, { 81, 5 } },
	{ 0x0200000C, { 86, 7 } },
	{ 0x0200000D, { 93, 3 } },
	{ 0x0200000E, { 96, 7 } },
	{ 0x0200000F, { 103, 4 } },
	{ 0x02000010, { 107, 21 } },
	{ 0x02000012, { 128, 2 } },
	{ 0x06000005, { 0, 10 } },
	{ 0x06000006, { 10, 5 } },
	{ 0x06000007, { 15, 2 } },
	{ 0x06000008, { 17, 1 } },
	{ 0x06000009, { 18, 3 } },
	{ 0x0600000A, { 21, 2 } },
	{ 0x0600000B, { 23, 4 } },
	{ 0x0600000C, { 27, 4 } },
	{ 0x0600000D, { 31, 3 } },
	{ 0x0600000E, { 34, 1 } },
	{ 0x0600000F, { 35, 3 } },
	{ 0x06000010, { 38, 2 } },
	{ 0x06000011, { 40, 5 } },
	{ 0x0600002F, { 79, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[130] = 
{
	{ (Il2CppRGCTXDataType)2, 18014 },
	{ (Il2CppRGCTXDataType)3, 12888 },
	{ (Il2CppRGCTXDataType)2, 18015 },
	{ (Il2CppRGCTXDataType)2, 18016 },
	{ (Il2CppRGCTXDataType)3, 12889 },
	{ (Il2CppRGCTXDataType)2, 18017 },
	{ (Il2CppRGCTXDataType)2, 18018 },
	{ (Il2CppRGCTXDataType)3, 12890 },
	{ (Il2CppRGCTXDataType)2, 18019 },
	{ (Il2CppRGCTXDataType)3, 12891 },
	{ (Il2CppRGCTXDataType)2, 18020 },
	{ (Il2CppRGCTXDataType)3, 12892 },
	{ (Il2CppRGCTXDataType)3, 12893 },
	{ (Il2CppRGCTXDataType)2, 13466 },
	{ (Il2CppRGCTXDataType)3, 12894 },
	{ (Il2CppRGCTXDataType)2, 18021 },
	{ (Il2CppRGCTXDataType)3, 12895 },
	{ (Il2CppRGCTXDataType)3, 12896 },
	{ (Il2CppRGCTXDataType)2, 18022 },
	{ (Il2CppRGCTXDataType)3, 12897 },
	{ (Il2CppRGCTXDataType)3, 12898 },
	{ (Il2CppRGCTXDataType)2, 13482 },
	{ (Il2CppRGCTXDataType)3, 12899 },
	{ (Il2CppRGCTXDataType)2, 18023 },
	{ (Il2CppRGCTXDataType)2, 18024 },
	{ (Il2CppRGCTXDataType)2, 13483 },
	{ (Il2CppRGCTXDataType)2, 18025 },
	{ (Il2CppRGCTXDataType)2, 18026 },
	{ (Il2CppRGCTXDataType)2, 18027 },
	{ (Il2CppRGCTXDataType)2, 13485 },
	{ (Il2CppRGCTXDataType)2, 18028 },
	{ (Il2CppRGCTXDataType)2, 13487 },
	{ (Il2CppRGCTXDataType)2, 18029 },
	{ (Il2CppRGCTXDataType)3, 12900 },
	{ (Il2CppRGCTXDataType)2, 13490 },
	{ (Il2CppRGCTXDataType)2, 13492 },
	{ (Il2CppRGCTXDataType)2, 18030 },
	{ (Il2CppRGCTXDataType)3, 12901 },
	{ (Il2CppRGCTXDataType)2, 18031 },
	{ (Il2CppRGCTXDataType)3, 12902 },
	{ (Il2CppRGCTXDataType)3, 12903 },
	{ (Il2CppRGCTXDataType)2, 18032 },
	{ (Il2CppRGCTXDataType)2, 13497 },
	{ (Il2CppRGCTXDataType)2, 18033 },
	{ (Il2CppRGCTXDataType)2, 13499 },
	{ (Il2CppRGCTXDataType)3, 12904 },
	{ (Il2CppRGCTXDataType)3, 12905 },
	{ (Il2CppRGCTXDataType)2, 13502 },
	{ (Il2CppRGCTXDataType)3, 12906 },
	{ (Il2CppRGCTXDataType)3, 12907 },
	{ (Il2CppRGCTXDataType)2, 13511 },
	{ (Il2CppRGCTXDataType)2, 18034 },
	{ (Il2CppRGCTXDataType)3, 12908 },
	{ (Il2CppRGCTXDataType)3, 12909 },
	{ (Il2CppRGCTXDataType)2, 13513 },
	{ (Il2CppRGCTXDataType)2, 17930 },
	{ (Il2CppRGCTXDataType)3, 12910 },
	{ (Il2CppRGCTXDataType)3, 12911 },
	{ (Il2CppRGCTXDataType)3, 12912 },
	{ (Il2CppRGCTXDataType)2, 13520 },
	{ (Il2CppRGCTXDataType)2, 18035 },
	{ (Il2CppRGCTXDataType)3, 12913 },
	{ (Il2CppRGCTXDataType)3, 12914 },
	{ (Il2CppRGCTXDataType)3, 12352 },
	{ (Il2CppRGCTXDataType)3, 12915 },
	{ (Il2CppRGCTXDataType)3, 12916 },
	{ (Il2CppRGCTXDataType)2, 13529 },
	{ (Il2CppRGCTXDataType)2, 18036 },
	{ (Il2CppRGCTXDataType)3, 12917 },
	{ (Il2CppRGCTXDataType)3, 12918 },
	{ (Il2CppRGCTXDataType)3, 12919 },
	{ (Il2CppRGCTXDataType)3, 12920 },
	{ (Il2CppRGCTXDataType)3, 12921 },
	{ (Il2CppRGCTXDataType)3, 12358 },
	{ (Il2CppRGCTXDataType)3, 12922 },
	{ (Il2CppRGCTXDataType)3, 12923 },
	{ (Il2CppRGCTXDataType)2, 18037 },
	{ (Il2CppRGCTXDataType)3, 12924 },
	{ (Il2CppRGCTXDataType)3, 12925 },
	{ (Il2CppRGCTXDataType)2, 18038 },
	{ (Il2CppRGCTXDataType)3, 12926 },
	{ (Il2CppRGCTXDataType)2, 18039 },
	{ (Il2CppRGCTXDataType)3, 12927 },
	{ (Il2CppRGCTXDataType)3, 12928 },
	{ (Il2CppRGCTXDataType)3, 12929 },
	{ (Il2CppRGCTXDataType)2, 13561 },
	{ (Il2CppRGCTXDataType)3, 12930 },
	{ (Il2CppRGCTXDataType)2, 13569 },
	{ (Il2CppRGCTXDataType)3, 12931 },
	{ (Il2CppRGCTXDataType)2, 18040 },
	{ (Il2CppRGCTXDataType)2, 18041 },
	{ (Il2CppRGCTXDataType)3, 12932 },
	{ (Il2CppRGCTXDataType)3, 12933 },
	{ (Il2CppRGCTXDataType)3, 12934 },
	{ (Il2CppRGCTXDataType)3, 12935 },
	{ (Il2CppRGCTXDataType)3, 12936 },
	{ (Il2CppRGCTXDataType)3, 12937 },
	{ (Il2CppRGCTXDataType)2, 13585 },
	{ (Il2CppRGCTXDataType)2, 18042 },
	{ (Il2CppRGCTXDataType)3, 12938 },
	{ (Il2CppRGCTXDataType)3, 12939 },
	{ (Il2CppRGCTXDataType)2, 13589 },
	{ (Il2CppRGCTXDataType)3, 12940 },
	{ (Il2CppRGCTXDataType)2, 18043 },
	{ (Il2CppRGCTXDataType)2, 13599 },
	{ (Il2CppRGCTXDataType)2, 13597 },
	{ (Il2CppRGCTXDataType)2, 18044 },
	{ (Il2CppRGCTXDataType)3, 12941 },
	{ (Il2CppRGCTXDataType)2, 18045 },
	{ (Il2CppRGCTXDataType)3, 12942 },
	{ (Il2CppRGCTXDataType)3, 12943 },
	{ (Il2CppRGCTXDataType)3, 12944 },
	{ (Il2CppRGCTXDataType)2, 13603 },
	{ (Il2CppRGCTXDataType)3, 12945 },
	{ (Il2CppRGCTXDataType)3, 12946 },
	{ (Il2CppRGCTXDataType)2, 13606 },
	{ (Il2CppRGCTXDataType)3, 12947 },
	{ (Il2CppRGCTXDataType)1, 18046 },
	{ (Il2CppRGCTXDataType)2, 13605 },
	{ (Il2CppRGCTXDataType)3, 12948 },
	{ (Il2CppRGCTXDataType)1, 13605 },
	{ (Il2CppRGCTXDataType)1, 13603 },
	{ (Il2CppRGCTXDataType)2, 18047 },
	{ (Il2CppRGCTXDataType)2, 13605 },
	{ (Il2CppRGCTXDataType)3, 12949 },
	{ (Il2CppRGCTXDataType)3, 12950 },
	{ (Il2CppRGCTXDataType)3, 12951 },
	{ (Il2CppRGCTXDataType)2, 13604 },
	{ (Il2CppRGCTXDataType)3, 12952 },
	{ (Il2CppRGCTXDataType)2, 13617 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	95,
	s_methodPointers,
	s_InvokerIndices,
	0,
	NULL,
	27,
	s_rgctxIndices,
	130,
	s_rgctxValues,
	NULL,
};
